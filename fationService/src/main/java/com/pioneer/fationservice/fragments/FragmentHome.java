package com.pioneer.fationservice.fragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import net.tsz.afinal.FinalActivity;
import net.tsz.afinal.FinalHttp;
import net.tsz.afinal.annotation.view.ViewInject;
import net.tsz.afinal.http.AjaxCallBack;
import net.tsz.afinal.http.AjaxParams;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.pioneer.fationservice.R;
import com.pioneer.fationservice.adapters.ShopInfoListAdapter;
import com.pioneer.fationservice.commons.SystemData;
import com.pioneer.fationservice.commons.SystemHttpURL;
import com.pioneer.fationservice.entries.ShopInfoListEntry;
import com.pioneer.fationservice.models.ShopInfoListModel;
import com.pioneer.fationservice.parsers.ShopInfoListParser;
import com.pioneer.fationservice.receivers.LocationBroadcastReceiver;
import com.pioneer.fationservice.receivers.LocationBroadcastReceiver.OnLocationChangedListener;
import com.pioneer.fationservice.serviceinfos.SearchResultActivity;
import com.pioneer.fationservice.serviceinfos.ShopDetailActivity;
import com.pioneer.fationservice.services.LocationService;

/**
 * 首页Fragment
 * @author Mrper
 *
 */
public class FragmentHome extends Fragment implements OnItemClickListener{

	public static final String TAG = "FragmentHome";
	@ViewInject(id=R.id.layout_actionbar_txtTitle) private TextView txtTitle;//标题栏
	@ViewInject(id=R.id.fragment_home_txtLocation) private TextView txtLocation;//当前位置文本
	@ViewInject(id=R.id.fragment_home_etSearch) private EditText etSearch;//搜索框
	@ViewInject(id=R.id.fragment_home_imgSearch,click="onSearchClick") private ImageView imgSearch;//搜索按钮
	@ViewInject(id=R.id.fragment_home_txtFood,click="onTypeClick") private TextView txtFood;//美食 1
	@ViewInject(id=R.id.fragment_home_txtHotel,click="onTypeClick") private TextView txtHotel;//酒店 2
	@ViewInject(id=R.id.fragment_home_txtTourist,click="onTypeClick") private TextView txtTourist;//景点 3
	@ViewInject(id=R.id.fragment_home_txtRepirAddr,click="onTypeClick") private TextView txtRepairAddr;//维修点
	@ViewInject(id=R.id.fragment_home_plLst,click="onTypeClick") private PullToRefreshListView plLst;//刷新列表控件
	
	private LocationBroadcastReceiver locationReceiver;//位置广播器
	private LocationService locationService;//位置服务
	private ServiceConnection serviceConn;//服务连接器
	private int pageIndex = 0;//当前页码
	private int flag = 0;//商家类型
	private ProgressDialog dialog;//进度提示框
	private List<ShopInfoListEntry> shopInfoList = new ArrayList<ShopInfoListEntry>();//商家列表
	private ShopInfoListAdapter adapter;//商家数据适配器
	
	public FragmentHome() {
	}
	
	@SuppressLint("InflateParams")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		View viewRoot = inflater.inflate(R.layout.fragment_home, null);
		FinalActivity.initInjectedView(this, viewRoot);
		return viewRoot;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		initLocationService();//初始化地理服务
		initLocationReceiver();//注册地理广播器
		//初始化参数
		txtTitle.setText("首页");
		txtLocation.setText("当前位置："+SystemData.myCurrentLocation);
		plLst.setMode(Mode.BOTH);//设置可以上拉，可以下啦
		plLst.setOnRefreshListener(new OnRefreshListener2<ListView>() {
			@Override
			public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
				requestShopList(0,false,true);//请求商家列表
			}
			@Override
			public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
				requestShopList(pageIndex+1,false,false);//请求商家列表
			}
		});
		adapter  = new ShopInfoListAdapter(getActivity(),shopInfoList);//商家地址
		plLst.setAdapter(adapter);
		plLst.setOnItemClickListener(this);
		requestShopList(0,true,false);//请求商家列表
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,long id) {
		startActivity(new Intent(getActivity(),ShopDetailActivity.class));
		getActivity().overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
	}
	
	/***
	 * 类型点击
	 * @param view
	 */
	public void onTypeClick(View view){
		switch(view.getId()){
		case R.id.fragment_home_txtFood:
			flag = 1;
			break;
		case R.id.fragment_home_txtHotel:
			flag = 2;
			break;
		case R.id.fragment_home_txtTourist:
			flag = 3;
			break;
		case R.id.fragment_home_txtRepirAddr:
			flag = 4;
			break;
		}
		requestShopList(0,true,true);//请求商家列表
	}
	
	/**
	 * 搜索点击事件
	 * @param view
	 */
	public void onSearchClick(View view){
		String searchKey = etSearch.getText().toString().trim();
		if(TextUtils.isEmpty(searchKey)){
			Toast toast = Toast.makeText(getActivity(), "搜索的关键字不能为空!", Toast.LENGTH_SHORT);
			toast.setGravity(Gravity.CENTER, 0, 0);
			toast.show();
			return;
		}
		Intent intent = new Intent(getActivity(),SearchResultActivity.class);
		intent.putExtra("searchKey", searchKey);
		startActivity(intent);
		getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}
	
	/**
	 * 请求商家列表
	 */
	public void requestShopList(final int pageIndex,final boolean isFirstQuery,final boolean isClearList){
		final Activity activity = getActivity();
		if(isFirstQuery) dialog = ProgressDialog.show(activity, "", "正在获取数据");
    	FinalHttp finalHttp = new FinalHttp();
    	finalHttp.configCharset("UTF-8");
    	finalHttp.configTimeout(10*1000);
    	finalHttp.addHeader("Content-Type", "html/text");
    	finalHttp.addHeader("Conection", "Keep-Alive");
    	finalHttp.addHeader("Cache-Control", "no-cache");
    	HashMap<String,String> params = new HashMap<String,String>();
    	params.put("z", SystemData.myLatLng.longitude+","+SystemData.myLatLng.latitude);
    	params.put("pageIndex", String.valueOf(pageIndex));
    	params.put("f", String.valueOf(flag));
    	params.put("authkey", SystemData.HTTP_KEY);
    	finalHttp.post(SystemHttpURL.URL_SHOP_LIST, new AjaxParams(params),new AjaxCallBack<String>(){
    		@Override
    		public void onFailure(Throwable t, int errorNo, String strMsg) {
    			super.onFailure(t, errorNo, strMsg);
    			if(dialog!=null) dialog.dismiss();
    			if(plLst.isRefreshing()) plLst.onRefreshComplete();
    			Toast.makeText(activity, "网络错误，请重试!", Toast.LENGTH_SHORT).show();
    		}
    		@Override
    		public void onSuccess(String str) {
    			super.onSuccess(str);
    			if(dialog!=null) dialog.dismiss();
    			ShopInfoListModel model = (ShopInfoListModel) new ShopInfoListParser(str).getResponseObject();
    			switch(model.status){
    			case 1:
    				if(isClearList) shopInfoList.clear();
    				shopInfoList.addAll(model.shopLists);
    				adapter.notifyDataSetChanged();
    				FragmentHome.this.pageIndex = pageIndex;//成功后获取当前页码
    				break;
    			case 0:
    				Toast.makeText(activity, model.msg, Toast.LENGTH_SHORT).show();
    				break;
    			}
    			if(plLst.isRefreshing()) plLst.onRefreshComplete();
    		}
    	});
	}
	
	/**
	 * 初始化地理服务
	 */
	public void initLocationService(){
		serviceConn = new ServiceConnection() {
			@Override
			public void onServiceDisconnected(ComponentName name) {
				locationService = null;
			}
			@Override
			public void onServiceConnected(ComponentName name, IBinder service) {
				locationService = ((LocationService.LocationBinder)service).getServiceInstance();
			}
		};//绑定并启动服务
		getActivity().bindService(new Intent(getActivity(),LocationService.class), serviceConn, Context.BIND_AUTO_CREATE);
		
	}
	
	/**
	 * 初始化位置广播器
	 */
	public void initLocationReceiver(){
		locationReceiver = new LocationBroadcastReceiver();
		locationReceiver.setOnLocationChangedListener(new OnLocationChangedListener() {
			@Override
			public void locationChanged(String location) {
				txtLocation.setText("当前位置："+ location);
			}
		});
		IntentFilter filter = new IntentFilter(LocationBroadcastReceiver.ACTION_TAG);
		getActivity().registerReceiver(locationReceiver,filter);
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		getActivity().unregisterReceiver(locationReceiver);
		getActivity().unbindService(serviceConn);
	}
	
	
}
