package com.pioneer.fationservice.fragments;

import net.tsz.afinal.FinalActivity;
import net.tsz.afinal.annotation.view.ViewInject;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.pioneer.fationservice.R;
import com.pioneer.fationservice.commons.SystemData;
import com.pioneer.fationservice.helpers.FileHelper;

/**
 * 首页页面
 * @author Mrper
 *
 */
public class FragmentMore extends Fragment {

	public static final String TAG = "FragmentMore";
	
	@ViewInject(id=R.id.layout_actionbar_txtTitle) private TextView txtTitle;//标题文本栏
	@ViewInject(id=R.id.fragment_more_txtIntroduce,click="onItemClick") private TextView txtIntroduce;//软件介绍
	@ViewInject(id=R.id.fragment_more_txtClean,click="onItemClick") private TextView txtClean;//清理垃圾
	@ViewInject(id=R.id.fragment_more_txtUpdate,click="onItemClick") private TextView txtUpdate;//软件更新
	@ViewInject(id=R.id.fragment_more_txtAbout,click="onItemClick") private TextView txtAbout;//关于
	@ViewInject(id=R.id.fragment_more_txtSettings,click="onItemClick") private TextView txtSettings;//设置
	
	public static final int MSG_CLEAR_CACHE_DONE = 0x00012;//清除缓存成功
	
	private Handler handler = new Handler(){
		public void handleMessage(android.os.Message msg) {
			switch(msg.what){
			case MSG_CLEAR_CACHE_DONE:
				Toast.makeText(getActivity(), "清理缓存成功！", Toast.LENGTH_SHORT).show();
				break;
			}
		};
	};
	
	@SuppressLint("InflateParams")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		View viewRoot = inflater.inflate(R.layout.fragment_more, null);
		FinalActivity.initInjectedView(this, viewRoot);
		return viewRoot;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		txtTitle.setText("更多功能");
	}
	
	/**
	 * 每一项的点击事件
	 * @param view
	 */
	public void onItemClick(View view){
		switch(view.getId()){
		case R.id.fragment_more_txtIntroduce://软件介绍
			break;
		case R.id.fragment_more_txtSettings://软件设置
			break;
		case R.id.fragment_more_txtClean://清除缓存
			clearCache();
			break;
		case R.id.fragment_more_txtUpdate://软件更新
			break;
		case R.id.fragment_more_txtAbout://软件关于
			break;
		}
	}
	
	/**
	 * 清除缓存
	 */
	public void clearCache(){
		final ProgressDialog dialog = ProgressDialog.show(getActivity(), "", "正在清理缓存...");
		new Thread(new Runnable() {
			@Override
			public void run() {
				FileHelper.clearCache(SystemData.CACHE_DIR);
				if(dialog!=null) dialog.cancel();
				handler.sendEmptyMessage(MSG_CLEAR_CACHE_DONE);
			}
		}).start();
	}
	
}
