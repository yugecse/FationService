package com.pioneer.fationservice.fragments;

import net.tsz.afinal.FinalActivity;
import net.tsz.afinal.annotation.view.ViewInject;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.pioneer.fationservice.R;
import com.pioneer.fationservice.helpers.ImageHelper;
import com.pioneer.fationservice.helpers.PhoneHelper;
import com.pioneer.fationservice.serviceinfos.DocListActivity;
import com.pioneer.fationservice.serviceinfos.RecommentActivity;
import com.pioneer.fationservice.serviceinfos.SearchResultActivity;
import com.pioneer.fationservice.serviceinfos.ShopDetailActivity;
import com.pioneer.fationservice.serviceinfos.ShoplstInfoActivity;
import com.pioneer.fationservice.services.LocationService;

/**
 * 首页Fragment
 * @author Mrper
 *
 */
public class FragmentMain extends Fragment implements OnItemClickListener{
	
	/**
	 * 选中维修点
	 * @author Mrper
	 *
	 */
	public interface onSelectedTouristListener{
		public void onSelectedTour();
	}

	public static final String TAG = "FragmentMain";
	//@ViewInject(id=R.id.fragment_home_txtLocation) private TextView txtLocation;//当前位置文本
	@ViewInject(id=R.id.fragment_main_etSearch) private EditText etSearch;//搜索框
	@ViewInject(id=R.id.fragment_main_imgLogo) private ImageView imgLogo;//Logo图片
	@ViewInject(id=R.id.fragment_main_imgSearch,click="onSearchClick") private ImageView imgSearch;//搜索按钮
	@ViewInject(id=R.id.fragment_main_llFoods,click="onTypeClick") private LinearLayout llFood;//美食 1
	@ViewInject(id=R.id.fragment_main_llHotel,click="onTypeClick") private LinearLayout llHotel;//酒店 2
	@ViewInject(id=R.id.fragment_main_llTuorist,click="onTypeClick") private LinearLayout llTourist;//景点 3
	@ViewInject(id=R.id.fragment_main_llRepairStore,click="onTypeClick") private LinearLayout llRepairAddr;//维修点
	@ViewInject(id=R.id.fragment_main_llDriveSafe,click="onTypeClick") private LinearLayout llDriveSafe;//行车安全
	@ViewInject(id=R.id.fragment_main_llProblem,click="onTypeClick") private LinearLayout llSolveProblem;//故障应急
	@ViewInject(id=R.id.fragment_main_llCarInfos,click="onTypeClick") private LinearLayout llCarInfos;//汽车小常识
	@ViewInject(id=R.id.fragment_main_llRecomment,click="onTypeClick") private LinearLayout llRecomment;//投诉建议
	
	private LocationService locationService;//位置服务
	private ServiceConnection serviceConn;//服务连接器
	private onSelectedTouristListener listener;///
	
	public FragmentMain() {
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		//使用接口强制转换
		listener = (onSelectedTouristListener) activity;
		if(listener == null) 
			Log.e("FragmentMain","请让调用该Fragment的activity实现onSelectedTouristListener");
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		View viewRoot = inflater.inflate(R.layout.fragment_main, null);
		FinalActivity.initInjectedView(this, viewRoot);
		return viewRoot;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		initLocationService();//初始化地理服务
		adjustLogoImage();//调整LOGO图片
//		initLocationReceiver();//注册地理广播器
		//初始化参数
		//txtLocation.setText("当前位置："+SystemData.myCurrentLocation);
	}
	
	/**
	 * 调整LOGO图片
	 */
	public void adjustLogoImage(){
		Bitmap bmpOrg = ((BitmapDrawable)getActivity().getResources()
				.getDrawable(R.drawable.ic_main_logo)).getBitmap();
		imgLogo.setImageBitmap(ImageHelper.scaleBitmap(bmpOrg, 
				PhoneHelper.getScreenInfo(getActivity()).widthPixels));
	}
	
	/***
	 * 类型点击
	 * @param view
	 */
	public void onTypeClick(View view){
		switch(view.getId()){
		case R.id.fragment_main_llFoods://美食
			goDocListActivity(1,ShoplstInfoActivity.class);
			break;
		case R.id.fragment_main_llHotel://酒店 
			goDocListActivity(2,ShoplstInfoActivity.class);
			break;
		case R.id.fragment_main_llTuorist://景点
			goDocListActivity(3,ShoplstInfoActivity.class);
			break;
		case R.id.fragment_main_llRepairStore://维修店
			if(listener!=null) listener.onSelectedTour();
			break;
		case R.id.fragment_main_llDriveSafe://行车安全
			goDocListActivity(1,DocListActivity.class);
			break;
		case R.id.fragment_main_llProblem://故障应急
			goDocListActivity(3,DocListActivity.class);
			break;
		case R.id.fragment_main_llCarInfos://汽车小常识
			goDocListActivity(2,DocListActivity.class);
			break;
		case R.id.fragment_main_llRecomment://投诉建议
			startActivity(new Intent(getActivity(),RecommentActivity.class));
			break;
		}
	}
	
	/**
	 * 跳转到其他页面
	 * @param flag
	 */
	public void goDocListActivity(int flag,Class<? extends Activity> target){
		Intent intent = new Intent(getActivity(),target);
		intent.putExtra("flag", flag);
		startActivity(intent);
		getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}
	

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,long id) {
		startActivity(new Intent(getActivity(),ShopDetailActivity.class));
		getActivity().overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
	}

	/**
	 * 搜索点击事件
	 * @param view
	 */
	public void onSearchClick(View view){
		String searchKey = etSearch.getText().toString().trim();
		if(TextUtils.isEmpty(searchKey)){
			Toast toast = Toast.makeText(getActivity(), "搜索的关键字不能为空!", Toast.LENGTH_SHORT);
			toast.setGravity(Gravity.CENTER, 0, 0);
			toast.show();
			return;
		}
		Intent intent = new Intent(getActivity(),SearchResultActivity.class);
		intent.putExtra("searchKey", searchKey);
		startActivity(intent);
		getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}
	

	/**
	 * 初始化地理服务
	 */
	public void initLocationService(){
		serviceConn = new ServiceConnection() {
			@Override
			public void onServiceDisconnected(ComponentName name) {
				locationService = null;
			}
			@Override
			public void onServiceConnected(ComponentName name, IBinder service) {
				locationService = ((LocationService.LocationBinder)service).getServiceInstance();
			}
		};//绑定并启动服务
		getActivity().bindService(new Intent(getActivity(),LocationService.class), serviceConn, Context.BIND_AUTO_CREATE);
		
	}
	
//	/**
//	 * 初始化位置广播器
//	 */
//	public void initLocationReceiver(){
//		locationReceiver = new LocationBroadcastReceiver();
//		locationReceiver.setOnLocationChangedListener(new OnLocationChangedListener() {
//			@Override
//			public void locationChanged(String location) {
//				txtLocation.setText("当前位置："+ location);
//			}
//		});
//		IntentFilter filter = new IntentFilter(LocationBroadcastReceiver.ACTION_TAG);
//		getActivity().registerReceiver(locationReceiver,filter);
//	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
//		getActivity().unregisterReceiver(locationReceiver);
		getActivity().unbindService(serviceConn);
	}
	
	
}
