package com.pioneer.fationservice.fragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import net.tsz.afinal.FinalActivity;
import net.tsz.afinal.FinalHttp;
import net.tsz.afinal.annotation.view.ViewInject;
import net.tsz.afinal.http.AjaxCallBack;
import net.tsz.afinal.http.AjaxParams;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.pioneer.fationservice.R;
import com.pioneer.fationservice.adapters.ShopInfoListAdapter;
import com.pioneer.fationservice.commons.SystemData;
import com.pioneer.fationservice.commons.SystemHttpURL;
import com.pioneer.fationservice.entries.ShopInfoListEntry;
import com.pioneer.fationservice.models.ShopInfoListModel;
import com.pioneer.fationservice.parsers.ShopInfoListParser;
import com.pioneer.fationservice.receivers.LocationBroadcastReceiver;
import com.pioneer.fationservice.receivers.LocationBroadcastReceiver.OnLocationChangedListener;
import com.pioneer.fationservice.serviceinfos.ShopDetailActivity;

/**
 * 首页Fragment
 * @author Mrper
 *
 */
public class FragmentRepair extends Fragment implements OnItemClickListener{

	public static final String TAG = "FragmentRepair";
	
	@ViewInject(id=R.id.layout_actionbar_txtTitle) private TextView txtTitle;//标题文本栏
	@ViewInject(id=R.id.fragment_repair_plLst) PullToRefreshListView plLst;//维修店展示列表
	@ViewInject(id=R.id.fragment_repair_txtLocation) private TextView txtLocation;//当前位置文本
	
	private LocationBroadcastReceiver locationReceiver;//地理位置接收器
	private int pageIndex = 0;//当前页码
	private final int flag = 4;//维修点类型
	private ProgressDialog dialog;//进度提示框
	private List<ShopInfoListEntry> shopInfoList = new ArrayList<ShopInfoListEntry>();//商家列表
	private ShopInfoListAdapter adapter;//商家数据适配器
	
	@SuppressLint("InflateParams")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		View viewRoot = inflater.inflate(R.layout.fragment_repair, null);
		FinalActivity.initInjectedView(this, viewRoot);
		return viewRoot;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		txtTitle.setText("维修店");
		txtLocation.setText("当前位置："+SystemData.myCurrentLocation);
		initLocationReceiver();//注册地理接收器
		plLst.setMode(Mode.BOTH);//设置可以上拉，可以下啦
		plLst.setOnRefreshListener(new OnRefreshListener2<ListView>() {
			@Override
			public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
				requestShopList(1,false,true);//请求商家列表
			}
			@Override
			public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
				requestShopList(pageIndex+1,false,false);//请求商家列表
			}
		});
		adapter  = new ShopInfoListAdapter(getActivity(),shopInfoList);//商家地址
		plLst.setAdapter(adapter);
		plLst.setOnItemClickListener(this);
		requestShopList(1,true,true);//请求商家列表
	}
	
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,long id) {
		Intent intent = new Intent(getActivity(),ShopDetailActivity.class);
		final ShopInfoListEntry item = shopInfoList.get(position-1);
		intent.putExtra("shopId", item.shopId);
		intent.putExtra("flag", flag);
		startActivity(intent);
		getActivity().overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
	}
	
	/**
	 * 清空维修店列表
	 */
	public void clearRepairlst(){
		shopInfoList.clear();
		adapter.notifyDataSetChanged();
	}
	
	/**
	 * 初始化位置广播器
	 */
	public void initLocationReceiver(){
		locationReceiver = new LocationBroadcastReceiver();
		locationReceiver.setOnLocationChangedListener(new OnLocationChangedListener() {
			@Override
			public void locationChanged(String location) {
				txtLocation.setText("当前位置："+ location);
			}
		});
		IntentFilter filter = new IntentFilter(LocationBroadcastReceiver.ACTION_TAG);
		getActivity().registerReceiver(locationReceiver,filter);
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		getActivity().unregisterReceiver(locationReceiver);
	}
	
	/**
	 * 请求商家列表
	 */
	public void requestShopList(final int pageIndex,final boolean isFirstQuery,final boolean isClearlst){
		final Activity activity = getActivity();
		if(isFirstQuery) dialog = ProgressDialog.show(activity, "", "正在获取数据");
    	FinalHttp finalHttp = new FinalHttp();
    	finalHttp.configCharset("UTF-8");
    	finalHttp.configTimeout(10*1000);
    	finalHttp.addHeader("Content-Type", "html/text");
    	finalHttp.addHeader("Conection", "Keep-Alive");
    	finalHttp.addHeader("Cache-Control", "no-cache");
    	HashMap<String,String> params = new HashMap<String,String>();
    	params.put("z", SystemData.myLatLng.longitude+","+SystemData.myLatLng.latitude);
    	params.put("p", String.valueOf(pageIndex));
    	params.put("f", String.valueOf(flag));
    	params.put("authkey", SystemData.HTTP_KEY);
    	finalHttp.post(SystemHttpURL.URL_SHOP_LIST, new AjaxParams(params),new AjaxCallBack<String>(){
    		@Override
    		public void onFailure(Throwable t, int errorNo, String strMsg) {
    			super.onFailure(t, errorNo, strMsg);
    			if(dialog!=null) dialog.dismiss();
    			if(plLst.isRefreshing()) plLst.onRefreshComplete();
    			Toast.makeText(activity, "网络错误，请重试!", Toast.LENGTH_SHORT).show();
    		}
    		@Override
    		public void onSuccess(String str) {
    			super.onSuccess(str);
    			if(dialog!=null) dialog.dismiss();
    			ShopInfoListModel model = (ShopInfoListModel) new ShopInfoListParser(str).getResponseObject();
    			switch(model.status){
    			case 1:
    				if(isClearlst) clearRepairlst();
    				shopInfoList.addAll(model.shopLists);
    				adapter.notifyDataSetChanged();
    				FragmentRepair.this.pageIndex = pageIndex;//成功后获取当前页码
    				break;
    			case 0:
    				Toast.makeText(activity, model.msg, Toast.LENGTH_SHORT).show();
    				break;
    			}
    			if(plLst.isRefreshing()) plLst.onRefreshComplete();
    		}
    	});
	}	
	
}
