package com.pioneer.fationservice.helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class ConfigHelper {
	
	/**
	 * 默认配置文件名
	 */
	public static final String CONFIG_NAME = "sys.ini";
	
	private SharedPreferences config;
	
	private static ConfigHelper configHelper;
	
	/**
	 * 使用默认配置文件名实例化配置对象
	 * @param context
	 * @return
	 */
	public static ConfigHelper getInstance(Context context){
		if(configHelper == null) 
			configHelper = new ConfigHelper(context, CONFIG_NAME);
		return configHelper;
	}
	
	/**
	 * 构造函数
	 * @param context
	 * @param configName
	 */
	public ConfigHelper(Context context,String configName){
		this.config = context.getSharedPreferences(configName, Context.MODE_PRIVATE);
	}
	
	/**
	 * 获取编辑器对象
	 * @return
	 */
	public Editor getConfigEditor(){
		return this.config.edit();
	}
	
	/**
	 * 配置对象中是否包含了某个参数
	 * @param key
	 * @return
	 */
	public boolean isExists(String key){
		return this.config.contains(key);
	}
	
	/**
	 * 获取配置对象
	 * @return
	 */
	public SharedPreferences getConfiger(){
		return this.config;
	}
	
}
