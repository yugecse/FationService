package com.pioneer.fationservice.helpers;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;

/**
 * APK辅助类
 * @author Mrper
 *
 */
public class ApkHelper {
	
	/**
	 * 获取应用信息
	 * @param context
	 * @return
	 */
	public static ApplicationInfo getApplicationInfo(Context context){
		try{
			return context.getPackageManager().getApplicationInfo(context.getPackageName(), 0);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 获取包信息
	 * @param context
	 * @return
	 */
	public static PackageInfo getPackageInfo(Context context){
		try{
			return context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	/***
	 * 获取版本号
	 * @param context
	 * @return
	 */
	public static int getVersionInfoNumber(Context context){
		PackageInfo packageInfo = getPackageInfo(context);
		if(packageInfo!=null) return packageInfo.versionCode;
		return 0;
	}
	
	/**
	 * 获取版本标识
	 * @param context
	 * @return
	 */
	public static String getVersionInfo(Context context){
		PackageInfo packageInfo = getPackageInfo(context);
		if(packageInfo!=null) return packageInfo.versionName;
		return null;
	}
}
