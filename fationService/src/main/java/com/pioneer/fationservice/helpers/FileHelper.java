package com.pioneer.fationservice.helpers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import android.annotation.SuppressLint;
import com.pioneer.fationservice.commons.SystemData;

/***
 * 文件辅助类
 * @author Mrper
 *
 */
public class FileHelper {
	
	@SuppressLint("SdCardPath")
	public static String getStorageDirName(){
		File file = new File("/storage");
		if(file.exists() || file.isDirectory()){
			File[] files = file.listFiles();
			if(files.length>=1) return "/storage/sdcard0/";
		}
		return "/mnt/sdcard/";
	}
	
	/**
	 * 保存文件
	 * @return 成功返回TURE，失败返回FALSE
	 */
	public static boolean saveFile(InputStream stream,String filename) throws Exception{
		File file = new File(filename);
		if(file.exists()) file.delete();
		File fileParent = file.getParentFile();
		if(fileParent.mkdirs()||fileParent.isDirectory()){
			FileOutputStream fos = new FileOutputStream(file);
			int length = 0;
			byte[] buffer = new byte[2048];
			while((length = stream.read(buffer,0,buffer.length))!=-1)
				fos.write(buffer,0,length);
			fos.flush();
			stream.close();
			fos.close();
			return true;
		}
		return false;
	}
	
	/**
	 * 保存文件
	 * @param content 要保存的内容
	 * @param encoding 要保存的编码格式
	 * @param filename 保存的文件名
	 * @return
	 * @throws Exception
	 */
	public static boolean saveFile(String content,String encoding,String filename) throws Exception{
		File file = new File(filename);
		if(file.exists()) file.delete();
		File fileParent = file.getParentFile();
		if(fileParent.mkdirs()||fileParent.isDirectory()){
			FileOutputStream fos = new FileOutputStream(file);
			fos.write(content.getBytes(encoding));
			fos.close();
			return true;
		}
		return false;
	}
	
	/**
	 * 读取文件
	 * @param filename 要读取的文件名
	 * @return
	 */
	public static String readFile(String filename) throws Exception{
		File file = new File(filename);
		if(!file.exists()) return null;
		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
		StringBuilder strBuilder = new StringBuilder();
		String lineContent = null;
		while((lineContent = reader.readLine())!=null)
			strBuilder.append(lineContent);
		reader.close();
		return strBuilder.toString();
	}
	
	/**
	 * 读取数据流
	 * @param stream 数据流
	 * @param encoding 数据编码
	 * @return
	 * @throws Exception
	 */
	public static String getStreamString(InputStream stream,String encoding) throws Exception{
		if(stream == null) return null;
		StringBuilder strBuilder = new StringBuilder();
		BufferedReader br = new BufferedReader(new InputStreamReader(stream,encoding));
		String ret = null;
		while((ret = br.readLine())!=null)
			strBuilder.append(ret);
		br.close();
		return strBuilder.toString();
	}
	
	/**
	 * 清理缓存
	 */
	public static void clearCache(String cacheDir){
		File fileDir = new File(cacheDir);
		if(fileDir.isDirectory()){
			for(File fileItem : fileDir.listFiles()){
				try{
					clearCache(fileItem.getAbsolutePath());
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			try{
				if(!cacheDir.equals(SystemData.CACHE_DIR))
					fileDir.delete();
			}catch(Exception e){
				e.printStackTrace();
			}
		}else if(fileDir.isFile()){
			try{
				fileDir.delete();
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}
	
}
