package com.pioneer.fationservice.helpers;

import android.graphics.Bitmap;
import android.graphics.Matrix;

public class ImageHelper {
	/**
	 * 根据宽度缩放图
	 * 
	 * @param src
	 * @param width
	 * @return
	 */
	public static Bitmap scaleBitmap(Bitmap src, int width) {
		int scaleX = width;
		int scaleY = (int) (width / ((float) src.getWidth() / src.getHeight()));
		Matrix matrix = new Matrix();
		matrix.postScale((float) scaleX / src.getWidth(),
				(float) scaleY / src.getHeight());
		Bitmap newbm = Bitmap.createBitmap(src, 0, 0, src.getWidth(),
				src.getHeight(), matrix, true);
		return newbm;
	}

}
