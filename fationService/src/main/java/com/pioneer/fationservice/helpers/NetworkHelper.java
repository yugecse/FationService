package com.pioneer.fationservice.helpers;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * 网络连接辅助类
 * @author Mrper
 *
 */
public class NetworkHelper {
	
	/**
	 * 获取激活的网络信息
	 * @return
	 */
	public static NetworkInfo getActiveNetworkInfo(Context context){
		ConnectivityManager connManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
		return connManager.getActiveNetworkInfo();
	}
	
	/**
	 * 客户端连接互联网的网络是否可用
	 * @param context
	 * @return
	 */
	public static boolean checkActiveNetworkAvailable(Context context){
		NetworkInfo info = getActiveNetworkInfo(context);
		return info == null?false:info.isAvailable();
	}
	
}
