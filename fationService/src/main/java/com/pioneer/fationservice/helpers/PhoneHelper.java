package com.pioneer.fationservice.helpers;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.WindowManager;

/**
 * 电话操作辅助类
 * @author Mrper
 *
 */
public class PhoneHelper {

	/**
	 * 获取本机电话号码
	 * @param context
	 * @return 如果成功返回电话号码，否则返回NULL
	 */
	public static String getPhoneNumber(Context context){
		TelephonyManager telephoneManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		String phoneNumber =  telephoneManager.getLine1Number();
		return phoneNumber==null?null:(phoneNumber.matches("1[3|5|7|8|][0-9]{9}")?phoneNumber:null);
	}
	
	/**
	 * 获取屏幕尺寸信息
	 * @param context
	 * @return
	 */
	public static DisplayMetrics getScreenInfo(Context context){
		 WindowManager wm = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
		 DisplayMetrics dm = new DisplayMetrics();
		 wm.getDefaultDisplay().getMetrics(dm);
		 return dm;
	}
	
	/**
	 * 拨打电话
	 * @param number
	 */
	public static void callPhoneNumber(Context context,String number){
		Intent intent = new Intent(Intent.ACTION_CALL);
		intent.setData(Uri.parse("tel:"+number));
		context.startActivity(intent);
	}
	
	/**
	 * 获取手机唯一标识IMEI
	 * @param context
	 * @return
	 */
	public static String getPhoneIEMI(Context context){
		TelephonyManager telephoneManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		return telephoneManager.getDeviceId();
	}
	
}
