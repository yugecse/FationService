package com.pioneer.fationservice.adapters;

import java.util.List;

import net.tsz.afinal.FinalBitmap;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.pioneer.fationservice.R;
import com.pioneer.fationservice.commons.SystemData;
import com.pioneer.fationservice.entries.CollectInfoListEntry;

public class CollectionAdapter extends BaseAdapter {

	public interface OnCollectClickListener{
		public void onCollectClick(final int shopId,final int position);
	}
	
	private Context context;
	private List<CollectInfoListEntry> list;
	private OnCollectClickListener listener;
	
	public CollectionAdapter(Context context,List<CollectInfoListEntry> list,OnCollectClickListener listener){
		this.context = context;
		this.list = list;
		this.listener = listener;
	}
	
	@Override
	public int getCount() {
		return list == null?0:list.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		final CollectInfoListEntry item = list.get(position);
		if(convertView == null){
			holder = new ViewHolder();
			convertView = View.inflate(context, R.layout.lst_item_collection, null);
			holder.imgIcon = (ImageView)convertView.findViewById(R.id.lst_item_collection_imgInfo);
			holder.txtCall = (TextView)convertView.findViewById(R.id.lst_item_collection_txtCall);
			holder.txtCollect = (TextView)convertView.findViewById(R.id.lst_item_collection_txtCollect);
			holder.txtTitle = (TextView)convertView.findViewById(R.id.lst_item_collection_txtTitle);
			holder.txtAddr = (TextView)convertView.findViewById(R.id.lst_item_collection_txtAddr);
			holder.txtCollect.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if(listener!=null) listener.onCollectClick(item.shopId, position);
				}
			});
			convertView.setTag(holder);
		}else{
			holder = (ViewHolder)convertView.getTag();
		}
		holder.txtCall.setText("电话："+item.shopPhone);
		holder.txtAddr.setText("地址："+item.shopAddress);
		holder.txtTitle.setText(item.shopName);
		FinalBitmap.create(context).configDiskCachePath(SystemData.CACHE_DIR).display(holder.imgIcon, item.shopPic);
		return convertView;
	}
	
	class ViewHolder{
		public ImageView imgIcon;
		public TextView txtTitle,
						txtAddr,
						txtCollect,
						txtShare,
						txtCall;
	}

}
