package com.pioneer.fationservice.adapters;

import java.util.List;

import net.tsz.afinal.FinalBitmap;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

import com.pioneer.fationservice.commons.SystemData;
import com.pioneer.fationservice.helpers.PhoneHelper;

/***
 * 投诉建议页面图片数据适配器
 * @author Mrper
 *
 */
public class RecommentAdapter extends BaseAdapter {

	private Context context;
	private List<String> piclst;
	private int blockSize;
	
	public RecommentAdapter(Context context,List<String> piclst){
		this.context = context;
		this.piclst = piclst;
		int size = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 35,
				context.getResources().getDisplayMetrics());
		blockSize = (PhoneHelper.getScreenInfo(context).widthPixels - size)/4;
	}
	
	@Override
	public int getCount() {
		return piclst == null?0:piclst.size();
	}

	@Override
	public Object getItem(int position) {
		return piclst.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final String imgUrl = piclst.get(position);
		ImageView imgView = (ImageView) convertView;
		if(imgView == null){
			imgView = new ImageView(context);
			AbsListView.LayoutParams lp = new AbsListView.LayoutParams(blockSize,blockSize);
			imgView.setLayoutParams(lp);
			imgView.setScaleType(ScaleType.CENTER_CROP);
		}
		if(imgUrl == "--add--"){
			imgView.setImageResource(android.R.drawable.ic_menu_add);
		}else if(!TextUtils.isEmpty(imgUrl)){
			imgView.setImageDrawable(Drawable.createFromPath(imgUrl));
		}
		return imgView;
	}

}
