package com.pioneer.fationservice.adapters;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.pioneer.fationservice.R;
import com.pioneer.fationservice.entries.DoclstInfoEntry;

/**
 * 知识小文档数据适配器
 * @author Mrper
 *
 */
public class DoclstInfoAdapter extends BaseAdapter {

	private Context context;
	private List<DoclstInfoEntry> doclst;
	
	public DoclstInfoAdapter(Context context,List<DoclstInfoEntry> doclst){
		this.context = context;
		this.doclst = doclst;
	}
	
	@Override
	public int getCount() {
		return this.doclst == null?0:doclst.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO 自动生成的方法存根
		return this.doclst.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO 自动生成的方法存根
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		final DoclstInfoEntry item = doclst.get(position);
		if(convertView == null){
			convertView = View.inflate(context, R.layout.lst_item_doclist, null);
			holder = new ViewHolder();
			holder.txtTitle = (TextView)convertView.findViewById(R.id.lst_item_doclist_txtTitle);
			holder.txtDescription = (TextView)convertView.findViewById(R.id.lst_item_doclst_txtDescription);
			holder.txtTime = (TextView)convertView.findViewById(R.id.lst_item_doclst_txtTime);
			convertView.setTag(holder);
		}else{
			holder = (ViewHolder)convertView.getTag();
		}
		holder.txtTitle.setText(item.docTitle);
		holder.txtDescription.setText(item.docDescription);
		holder.txtTime.setText(item.docTime);
		return convertView;
	}
	
	
	class ViewHolder{
		public TextView txtTitle,//标题
						txtDescription,//描述
						txtTime;//时间
	}

}
