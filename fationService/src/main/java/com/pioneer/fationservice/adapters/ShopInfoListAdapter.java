package com.pioneer.fationservice.adapters;

import java.util.List;

import net.tsz.afinal.FinalBitmap;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.baidu.mapapi.utils.DistanceUtil;
import com.pioneer.fationservice.R;
import com.pioneer.fationservice.commons.SystemData;
import com.pioneer.fationservice.entries.ShopInfoListEntry;

/**
 * 首页数据适配器
 * @author Mrper
 *
 */
public class ShopInfoListAdapter extends BaseAdapter {

	private Context context;
	private List<ShopInfoListEntry> shoplist;
	
	public ShopInfoListAdapter(Context context,List<ShopInfoListEntry> shoplist){
		this.context = context;
		this.shoplist = shoplist;
	}
	
	@Override
	public int getCount() {
		return this.shoplist == null ? 0 : this.shoplist.size();
	}

	@Override
	public Object getItem(int position) {
		return this.shoplist.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		final ShopInfoListEntry item = this.shoplist.get(position);
		if(convertView == null){
			holder = new ViewHolder();
			convertView = View.inflate(context, R.layout.lst_item_shoplist, null);
			holder.imgIcon = (ImageView)convertView.findViewById(R.id.lst_item_home_imgInfo);
			holder.txtTitle = (TextView)convertView.findViewById(R.id.lst_item_home_txtTitle);
			holder.txtAddr = (TextView)convertView.findViewById(R.id.lst_item_home_txtAddr);
			holder.txtViewMap = (TextView)convertView.findViewById(R.id.lst_item_home_txtViewMap);
			holder.txtCall = (TextView)convertView.findViewById(R.id.lst_item_home_txtCall);
			holder.txtDistance = (TextView)convertView.findViewById(R.id.lst_item_home_txtDistance);
			convertView.setTag(holder);
		}else{
			holder = (ViewHolder)convertView.getTag();
		}
		holder.txtTitle.setText(item.shopName);
		holder.txtCall.setText(item.phone);
		holder.txtAddr.setText(item.address);
		FinalBitmap.create(context).configDiskCachePath(SystemData.CACHE_DIR).display(holder.imgIcon, item.picThumb);//图片下载
		System.out.println("图片地址："+item.picThumb);
		double distance = DistanceUtil.getDistance(SystemData.myLatLng, item.latlng);
		String distanceStr = "";
		if(distance >= 1000.00000){
			distanceStr = Math.round(distance/1000) +"km";
		}else{
			distanceStr = Math.round(distance)+"m";
		}
		holder.txtDistance.setText(distanceStr);
		return convertView;
	}
	
	class ViewHolder{
		public ImageView imgIcon;
		public TextView txtTitle,
						txtAddr,
						txtViewMap,
						txtCall,
						txtDistance;
	}

}
