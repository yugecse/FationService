package com.pioneer.fationservice.receivers;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.baidu.android.pushservice.CustomPushNotificationBuilder;
import com.baidu.android.pushservice.PushConstants;
import com.baidu.android.pushservice.PushManager;
import com.pioneer.fationservice.R;
import com.pioneer.fationservice.serviceinfos.ServiceMapActivity;

/**
 * 百度PUSH的信息接收器
 * @author Mrper
 *
 */
public class PushMessageReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		String action = intent.getAction();
		if(action.equals("com.baidu.android.pushservice.action.MESSAGE")){//把数据发送给客户端
			String message = intent.getExtras().getString(PushConstants.EXTRA_PUSH_MESSAGE_STRING);//消息
			//String title = intent.getExtras().getString(PushConstants.EXTRA_NOTIFICATION_TITLE);//标题
			Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
			showNotificationMessage(context,"最新推荐",message);//显示通知栏消息
		}//else if(action.equals("com.baidu.android.pushservice.action.RECEIVE")){//接口调用回调
//			
//		}else if(action.equals("com.baidu.android.pushservice.action.notification.CLICK")){//通知的点击事件回调
//			Toast.makeText(context, "你点击了通知栏的消息!", Toast.LENGTH_SHORT).show();
//		}
	}
	
	/**
	 * 显示通知栏消息
	 */
	@SuppressWarnings("deprecation")
	public void showNotificationMessage(Context context,String title,String message){
//		CustomPushNotificationBuilder cBuilder = new CustomPushNotificationBuilder(context,R.layout.layout_pushservice,  
//				R.id.layout_pushservice_imgIcon, R.id.layout_pushservice_txtTitle, R.id.layout_pushservice_txtContent);
//		cBuilder.setNotificationText(message);
//		cBuilder.setNotificationTitle(title);
//		cBuilder.setNotificationFlags(Notification.FLAG_AUTO_CANCEL);
//		cBuilder.setNotificationDefaults(Notification.DEFAULT_SOUND|Notification.DEFAULT_VIBRATE);
//		cBuilder.setStatusbarIcon(R.drawable.ic_launcher);
//		cBuilder.setLayoutDrawable(0);
//		PushManager.setNotificationBuilder(context, 1, cBuilder); 
		PendingIntent intentTarget = PendingIntent.getActivity(context, 0, new Intent(context,ServiceMapActivity.class), 0);
		Notification notification = new Notification(R.drawable.ic_launcher, message, System.currentTimeMillis());
		notification.setLatestEventInfo(context, title, message, intentTarget);
		notification.defaults = Notification.DEFAULT_ALL|Notification.DEFAULT_VIBRATE;
		notification.vibrate = new long[]{100,200,100};
		NotificationManager notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.notify(1, notification);
	}

}
