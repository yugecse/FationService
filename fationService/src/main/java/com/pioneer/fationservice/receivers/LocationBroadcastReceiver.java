package com.pioneer.fationservice.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * 位置BroadcastReceiver
 * @author Mrper
 *
 */
public class LocationBroadcastReceiver extends BroadcastReceiver{
	
	public interface OnLocationChangedListener{
		public void locationChanged(String location);
	}
	
	public final static String ACTION_TAG = "LocationBroadcastReceiver";
	
	private OnLocationChangedListener listener;
	
	public void setOnLocationChangedListener(OnLocationChangedListener listener){
		this.listener = listener;
	}
	
	@Override
	public void onReceive(Context context, Intent intent) {
		if(intent.getAction().equals(ACTION_TAG) && listener!=null){
			listener.locationChanged(intent.getStringExtra("addr"));
		}
	}
}