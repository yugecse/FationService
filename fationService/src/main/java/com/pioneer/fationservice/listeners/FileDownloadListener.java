package com.pioneer.fationservice.listeners;

/**
 * 文件下载监听器
 * @author Mrper
 *
 */
public interface FileDownloadListener {
	/**
	 * 文件开始下载
	 */
	public void fileDownloadStart();
	/**
	 * 文件正在下载
	 * @param percent
	 */
	public void fileDownloading(float percent);
	/**
	 * 文件下载完成
	 */
	public void fileDownDone();
	/**
	 * 文件下载出错
	 */
	public void fileDownloadError();
	
}
