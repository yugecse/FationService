package com.pioneer.fationservice;
import net.tsz.afinal.FinalActivity;
import net.tsz.afinal.annotation.view.ViewInject;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.text.TextUtils;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pioneer.fationservice.R.color;
import com.pioneer.fationservice.commons.SystemData;
import com.pioneer.fationservice.helpers.ConfigHelper;
import com.pioneer.fationservice.helpers.FileHelper;
import com.pioneer.fationservice.helpers.NetworkHelper;
import com.pioneer.fationservice.helpers.PhoneHelper;
import com.pioneer.fationservice.selfctls.IndictorView;
import com.pioneer.fationservice.selfctls.LeadingView;
import com.pioneer.fationservice.selfctls.LeadingView.SelectedChangedListener;
import com.pioneer.fationservice.serviceinfos.LoginActivity;
import com.pioneer.fationservice.serviceinfos.MainActivity;
import com.pioneer.fationservice.services.LocationService;

/**
 * Logo启动页面
 * @author Mrper
 *
 */
public class LogoActivity extends FinalActivity {

    @ViewInject(id=R.id.activity_logo_btnEnter,click="btnEnterClick") private TextView txtEnter;//进入主页的按钮
    @ViewInject(id=R.id.activity_logo_lvLeading) private LeadingView lvLogo;//启动图片展示组件
    @ViewInject(id=R.id.activity_logo_flContainer) private FrameLayout flContainer;//外部容器
    @ViewInject(id=R.id.activity_logo_ivIndictor) private IndictorView ivIndictor;//指示器
    
	private ServiceConnection serviceConn;//服务连接器
	private LocationService locationService;//位置服务
	
	/**
	 * 初始化地理服务
	 */
	public void initLocationService(){
		serviceConn = new ServiceConnection() {
			@Override
			public void onServiceDisconnected(ComponentName name) {
				locationService = null;
			}
			@Override
			public void onServiceConnected(ComponentName name, IBinder service) {
				locationService = ((LocationService.LocationBinder)service).getServiceInstance();
			}
		};//绑定并启动服务
		this.bindService(new Intent(this,LocationService.class), serviceConn, Context.BIND_AUTO_CREATE);
		
	}
	
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//        		WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_logo);
        initActivity();//初始化Activity
        initLocationService();//初始化地理位置服务
    }
    
    /**
     * 初始化Activity
     */
    public void initActivity(){
    	if(!NetworkHelper.checkActiveNetworkAvailable(this)){
    		new AlertDialog.Builder(this).setTitle("网络发生错误")
    			.setMessage("请检查您的网络是否通畅!")
    			.setPositiveButton("确定", new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						LogoActivity.this.finish();
						System.exit(0);
					}
				}).show();
    		return;
    	}
    	//获取系统缓存目录
    	SystemData.CACHE_DIR = FileHelper.getStorageDirName() + SystemData.CACHE_DIR;
    	lvLogo.setImageUrls(R.drawable.ic_logo2,R.drawable.ic_logo1,R.drawable.ic_logo3);
    	ivIndictor.setVisibility(View.VISIBLE);
    	ivIndictor.setIndictorCount(lvLogo.getImglstSize());
    	ivIndictor.setIndictorSelectedColor(color.orange);
    	lvLogo.setOnSelectedChangedListener(new SelectedChangedListener() {
			@Override
			public void selectedChanged(int selection) {
				ivIndictor.setIndictorSelectedIndex(selection);
				txtEnter.setVisibility(selection == lvLogo.getImglstSize()-1?View.VISIBLE:View.GONE);
			}
		});
    }

    /**
     * 进入按钮点击事件（跳转至主页面）
     * @param view
     */
    public void btnEnterClick(View view){
    	String imei = PhoneHelper.getPhoneIEMI(this);//获取手机的IMEI值
    	if(TextUtils.isEmpty(imei)){
    		Toast.makeText(this, "请允许获取读取手机状态，否则应用无法正常使用!", Toast.LENGTH_SHORT).show();
    		return;
    	}
    	ConfigHelper configer = ConfigHelper.getInstance(this);
    	if(!configer.isExists("IMEI") || TextUtils.isEmpty(configer.getConfiger().getString("IMEI", ""))){
    		configer.getConfigEditor().putString("IMEI", imei);
    	}
    	SystemData.HTTP_KEY = imei;//以此作为手机验证标识
    	System.out.println("手机的IMEI为："+imei);
//    	gotoLogin();//验证页面
    	//检测APP更新
    	//检测用户的验证
    	if(!configer.isExists("uid") || configer.getConfiger().getInt("uid", 0) == 0)//检测是否验证过手机
    		gotoLogin();
    	else{
    		SystemData.uid = configer.getConfiger().getInt("uid", 0);//从存储文件中获取用户ID
    		runApplication();
    	}
    		
    }
    
    /**
     * 启动APP
     */
    public void gotoLogin(){
    	startActivity(new Intent(LogoActivity.this,LoginActivity.class));
		LogoActivity.this.finish();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }
    
    /**
     * 直接运行运行应用程序
     */
    public void runApplication(){
    	startActivity(new Intent(LogoActivity.this,MainActivity.class));
		LogoActivity.this.finish();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }
    
    @Override
    protected void onDestroy() {
    	super.onDestroy();
    	this.unbindService(serviceConn);
    }
    
}
