package com.pioneer.fationservice.parsers;

import org.json.JSONArray;
import org.json.JSONObject;

import com.baidu.mapapi.model.LatLng;
import com.pioneer.fationservice.entries.MapShopListEntry;
import com.pioneer.fationservice.models.MapShopListModel;

/**
 * 地图商家列表解析器
 * @author Mrper
 *
 */
public class MapShopListParser extends BaseParser {

	private MapShopListModel model;
	
	public MapShopListParser(String requestResult) {
		super(MapShopListModel.class, requestResult);
	}

	@Override
	public void parseJSONModels(Object jsonModels, boolean isJSONObject) {
		if(jsonModels != null){
			try{
				model = (MapShopListModel) super.resObj;
				if(isJSONObject){
					model.mapShoplst.add(parserJSONObject((JSONObject)jsonModels));
				}else{
					JSONArray jArr = (JSONArray) jsonModels;
					for(int i=0;i<jArr.length();i++){
						model.mapShoplst.add(parserJSONObject(jArr.getJSONObject(i)));
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}
	
	public MapShopListModel getResponseObject(){
		return this.model;
	}
	
	private MapShopListEntry parserJSONObject(JSONObject jItem) throws Exception {
		MapShopListEntry item = new MapShopListEntry();
		if(jItem.has("sid"))
			item.shopId = Integer.valueOf(jItem.get("sid").toString());	
		if(jItem.has("x") && jItem.has("y"))
			item.shopLatLng = new LatLng(jItem.getDouble("y"),jItem.getDouble("x"));
		return item;
	}

}
