package com.pioneer.fationservice.parsers;

import org.json.JSONObject;

import com.pioneer.fationservice.models.DocinfoDetailModel;


/**
 * 文档小知识解析器
 * @author Mrper
 *
 */
public class DocinfoDetailParser extends BaseParser {

	private DocinfoDetailModel model;
	
	public DocinfoDetailParser(String requestResult) {
		super(DocinfoDetailModel.class, requestResult);
	}

	@Override
	public void parseJSONModels(Object jsonModels, boolean isJSONObject) {
		try{
			model = (DocinfoDetailModel) super.getResponseObject();
			if(jsonModels!=null){
				JSONObject jItem = (JSONObject) jsonModels;
				if(jItem.has("did")) model.docId = Integer.valueOf(jItem.get("did").toString());
				if(jItem.has("title")) model.content = jItem.getString("title");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public DocinfoDetailModel getResponseObject(){
		return model;
	}

}
