package com.pioneer.fationservice.parsers;

import org.json.JSONObject;

import com.pioneer.fationservice.models.ResponseObject;

/**
 * 网络访问结果Parser
 * @author Mrper
 *
 */
public abstract class BaseParser {
	
	protected JSONObject jsonRet;
	protected Object jsonModels;
	protected ResponseObject resObj;
	
	public BaseParser(Class<? extends ResponseObject> cls,String requestResult){
		try{
			String retJson = requestResult.substring(9,requestResult.length()-7);
			jsonRet = new JSONObject(retJson);
			resObj = cls.newInstance();//实例化模型类
			if(jsonRet.has("status"))
				resObj.status = jsonRet.getInt("status");
			if(jsonRet.has("msg"))
				resObj.msg = jsonRet.getString("msg");
			if(jsonRet.has("data")){
				jsonModels = jsonRet.opt("data");
				parseJSONModels(jsonModels,jsonModels instanceof JSONObject);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public abstract void parseJSONModels(Object jsonModels,boolean isJSONObject);
	
	public ResponseObject getResponseObject(){
		return this.resObj;
	}
}
