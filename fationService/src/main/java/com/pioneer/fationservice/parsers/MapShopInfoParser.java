package com.pioneer.fationservice.parsers;

import org.json.JSONObject;

import com.pioneer.fationservice.entries.MapShopInfoEntry;
import com.pioneer.fationservice.models.MapShopInfoModel;

/**
 * 地图商家弹窗信息解析器
 * @author Mrper
 *
 */
public class MapShopInfoParser extends BaseParser {

	private MapShopInfoModel model;
	
	public MapShopInfoParser(String requestResult) {
		super(MapShopInfoModel.class, requestResult);
	}

	@Override
	public void parseJSONModels(Object jsonModels, boolean isJSONObject) {
		model = (MapShopInfoModel) super.getResponseObject();
		try{
			if(super.jsonRet.has("imgdommain"))
				model.imgdommain = super.jsonRet.getString("imgdommain");
			JSONObject jItem = (JSONObject)jsonModels;
			model.mapShopInfoEntry = new MapShopInfoEntry();
			if(jItem.has("sid")) model.mapShopInfoEntry.shopId = jItem.getInt("sid");
			if(jItem.has("title")) model.mapShopInfoEntry.shopName =jItem.getString("title");
			if(jItem.has("des")) model.mapShopInfoEntry.shopDescription = jItem.getString("des");
			if(jItem.has("thumb")) model.mapShopInfoEntry.shopThumb = model.imgdommain + jItem.getString("thumb");
			model.mapShopInfoEntry.shopAddress = jItem.getString("province")+jItem.getString("city")
						+jItem.getString("district")+jItem.getString("street") + jItem.getString("num");
			if(jItem.has("tel")) model.mapShopInfoEntry.shopPhone = String.valueOf(jItem.getLong("tel"));
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public MapShopInfoModel getResponseObject(){
		return this.model;
	}

}
