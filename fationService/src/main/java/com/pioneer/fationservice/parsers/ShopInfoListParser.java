package com.pioneer.fationservice.parsers;

import org.json.JSONArray;
import org.json.JSONObject;

import com.baidu.mapapi.model.LatLng;
import com.pioneer.fationservice.entries.ShopInfoListEntry;
import com.pioneer.fationservice.models.ShopInfoListModel;


/**
 * 商家列表解析器
 * @author Mrper
 *
 */
public class ShopInfoListParser extends BaseParser {

	private ShopInfoListModel shopModel;
	
	public ShopInfoListParser(String requestResult) {
		super(ShopInfoListModel.class, requestResult);
	}
	
	@Override
	public void parseJSONModels(Object jsonModels,boolean isJSONObject) {
		shopModel = (ShopInfoListModel)super.getResponseObject();
		if(jsonModels!=null){
			try{
				if(jsonRet.has("imgdommain"))
					shopModel.picDomain = jsonRet.getString("imgdommain");
				if(isJSONObject){
					shopModel.shopLists.add(parseJsonObject((JSONObject)jsonModels));
				}else{
					JSONArray jArr = (JSONArray)jsonModels;
					for(int index = 0; index<jArr.length();index++)
						shopModel.shopLists.add(parseJsonObject(jArr.getJSONObject(index)));
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}
	
	private ShopInfoListEntry parseJsonObject(JSONObject jItem) throws Exception {
		ShopInfoListEntry item = new ShopInfoListEntry();
		if(jItem.has("sid")) item.shopId = jItem.getInt("sid");
		if(jItem.has("title")) item.shopName =jItem.getString("title");
		if(jItem.has("thumb")) item.picThumb = (shopModel!=null?shopModel.picDomain:"") + jItem.getString("thumb");
		if(jItem.has("x") && jItem.has("y")){
			item.latlng = new LatLng(jItem.getDouble("y"),jItem.getDouble("x"));
		}
		item.address = jItem.getString("province")+jItem.getString("city")
					+jItem.getString("district")+jItem.getString("street") + jItem.getString("num");
		if(jItem.has("tel")) item.phone = String.valueOf(jItem.getLong("tel"));
		return item;
	}
	
	public ShopInfoListModel getResponseObject(){
		return this.shopModel;
	}

}
