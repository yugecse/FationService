package com.pioneer.fationservice.parsers;

import org.json.JSONArray;
import org.json.JSONObject;

import com.pioneer.fationservice.entries.CollectInfoListEntry;
import com.pioneer.fationservice.models.CollectInfoListModel;

/**
 * 商家收藏列表解析器
 * @author Mrper
 *
 */
public class CollectInfoListParser extends BaseParser {

	private CollectInfoListModel model;
	
	public CollectInfoListParser(String requestResult) {
		super(CollectInfoListModel.class, requestResult);
	}

	@Override
	public void parseJSONModels(Object jsonModels, boolean isJSONObject) {
		try{
			this.model = (CollectInfoListModel) super.getResponseObject();
			if(jsonRet.has("imgdommain"))
				this.model.imgdommain = jsonRet.getString("imgdommain");
			if(isJSONObject){
				model.collectionInfoList.add(parserJSONObject((JSONObject)jsonModels));
			}else{
				JSONArray jArr = (JSONArray) jsonModels;
				for(int i=0;i<jArr.length();i++){
					model.collectionInfoList.add(parserJSONObject(jArr.getJSONObject(i)));
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private CollectInfoListEntry parserJSONObject(JSONObject jItem) throws Exception {
		CollectInfoListEntry item = new CollectInfoListEntry();
		if(jItem.has("sid"))
			item.shopId = Integer.valueOf(jItem.get("sid").toString());	
		if(jItem.has("title"))
			item.shopName = jItem.getString("title");
		if(jItem.has("tel"))
			item.shopPhone = jItem.getString("tel");
		if(jItem.has("thumb"))
			item.shopPic = this.model.imgdommain + jItem.getString("thumb");
		item.shopAddress = jItem.getString("province")+jItem.getString("city")
				+jItem.getString("district")+jItem.getString("street") + jItem.getString("num");
		return item;
	}

}
