package com.pioneer.fationservice.parsers;

import org.json.JSONArray;
import org.json.JSONObject;

import com.pioneer.fationservice.entries.DoclstInfoEntry;
import com.pioneer.fationservice.models.DoclstInfoModel;

/**
 * 文档列表解析器
 * @author Mrper
 *
 */
public class DoclstInfoParser extends BaseParser {

	private DoclstInfoModel model;
	
	public DoclstInfoParser(String requestResult) {
		super(DoclstInfoModel.class, requestResult);
	}

	@Override
	public void parseJSONModels(Object jsonModels, boolean isJSONObject) {
		try{
			model = (DoclstInfoModel) super.getResponseObject();
			if(isJSONObject){
				model.doclst.add(parserJSONObject((JSONObject)jsonModels));
			}else{
				if(jsonModels!=null){
					JSONArray jArr = (JSONArray) jsonModels;
					for(int i=0;i<jArr.length();i++){
						model.doclst.add(parserJSONObject(jArr.getJSONObject(i)));
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private DoclstInfoEntry parserJSONObject(JSONObject jItem) throws Exception{
		DoclstInfoEntry item = new DoclstInfoEntry();
		if(jItem.has("did")) item.docId = Integer.valueOf(jItem.get("did").toString());
		if(jItem.has("title")) item.docTitle = jItem.getString("title");
		if(jItem.has("des")) item.docDescription = jItem.getString("des");
		if(jItem.has("addymd")) item.docTime = jItem.getString("addymd");
		return item;
	}
	
	public DoclstInfoModel getResponseObject(){
		return this.model;
	}

}
