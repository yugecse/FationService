package com.pioneer.fationservice.parsers;

import com.pioneer.fationservice.models.VerfyCodeModel;

/**
 * 验证解析器
 * @author Mrper
 * 
 */
public class VerfyCodeParser extends BaseParser {

	public VerfyCodeModel model;
	
	public VerfyCodeParser(String requestResult) {
		super(VerfyCodeModel.class,requestResult);
		try{
			model = (VerfyCodeModel) super.getResponseObject();
			if(jsonRet.has("varcode")) model.verfyCode = jsonRet.getString("varcode");
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Override
	public void parseJSONModels(Object jsonModels,boolean isJSONObject) {
		
	}
	
	public VerfyCodeModel getResponseObject(){
		return this.model;
	}
	
}
