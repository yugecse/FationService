package com.pioneer.fationservice.parsers;

import org.json.JSONArray;
import org.json.JSONObject;

import com.baidu.mapapi.model.LatLng;
import com.pioneer.fationservice.entries.ShopInfoDetailEntry;
import com.pioneer.fationservice.models.ResponseObject;
import com.pioneer.fationservice.models.ShopInfoDetailModel;

/**
 * 商家详细信息数据解析器
 * @author Mrper
 *
 */
public class ShopInfoDetailParser extends BaseParser {

	private ShopInfoDetailModel model;
	
	public ShopInfoDetailParser(String requestResult) {
		super(ShopInfoDetailModel.class, requestResult);
	}

	@Override
	public void parseJSONModels(Object jsonModels,boolean isJSONObject) {
		try{
			model = (ShopInfoDetailModel) super.getResponseObject();
			if(jsonRet.has("imgdommain")) 
				model.imgDomain = jsonRet.getString("imgdommain");
			if(this.jsonModels!=null){
				JSONObject jItem = (JSONObject)jsonModels;
				model.shopInfoDetail = new ShopInfoDetailEntry();
				if(jItem.has("sid")) model.shopInfoDetail.shopId = jItem.getInt("sid");
				if(jItem.has("title")) model.shopInfoDetail.shopName = jItem.getString("title");
				if(jItem.has("des")) model.shopInfoDetail.shopDescription = jItem.getString("des");
				if(jItem.has("tel")) model.shopInfoDetail.shopPhone = jItem.getString("tel");
				if(jItem.has("isc")) model.shopInfoDetail.isCollect = (jItem.getInt("isc") == 1);
				if(jItem.has("service")) model.shopInfoDetail.shopServer = jItem.getString("service");
				if(jItem.has("jifen")) model.shopInfoDetail.shopScore = jItem.getString("jifen");
				if(jItem.has("ytime")) model.shopInfoDetail.shopTime = jItem.getString("ytime");
				if(jItem.has("yuyue")) model.shopInfoDetail.shopYuyue = jItem.getString("yuyue");
				if(jItem.has("tishi")) model.shopInfoDetail.shopTip = jItem.getString("tishi");
				if(jItem.has("web")) model.shopInfoDetail.shopHttp = jItem.getString("web");
				model.shopInfoDetail.shopAddress = jItem.getString("province")+jItem.getString("city")
							+jItem.getString("district")+jItem.getString("street") + jItem.getString("num");
				if(jItem.has("imglist")){
					JSONArray jArr = jItem.getJSONArray("imglist");
					if(jArr!=null && jArr.length()>0){
						for(int i=0;i<jArr.length();i++)
							model.shopInfoDetail.piclst.add(model.imgDomain 
									+ jArr.getJSONObject(i).getString("url"));
					}
				}
				if(jItem.has("x") && jItem.has("y")){
					model.shopInfoDetail.shopLatlng = new LatLng(jItem.getDouble("y"),jItem.getDouble("x"));
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public ShopInfoDetailModel getResponseObject(){
		return this.model;
	}

}
