package com.pioneer.fationservice.parsers;

import org.json.JSONArray;
import org.json.JSONObject;

import com.baidu.mapapi.model.LatLng;
import com.pioneer.fationservice.entry.ShopInfosEntry;
import com.pioneer.fationservice.models.ShopInfosModel;


/**
 * 商家列表解析器
 * @author Mrper
 *
 */
public class ShopInfoParser extends BaseParser {

	private ShopInfosModel shopModel;
	
	public ShopInfoParser(String requestResult) {
		super(ShopInfosModel.class, requestResult);
	}
	
	@Override
	public void parseJSONModels(Object jsonModels,boolean isJSONObject) {
		shopModel = (ShopInfosModel)super.getResponseObject();
		if(jsonModels!=null){
			try{
				if(jsonRet.has("imgdommain"))
					shopModel.picDomain = jsonRet.getString("imgdommain");
				if(isJSONObject){
					shopModel.shopLists.add(parseJsonObject((JSONObject)jsonModels));
				}else{
					JSONArray jArr = (JSONArray)jsonModels;
					for(int index = 0; index<jArr.length();index++)
						shopModel.shopLists.add(parseJsonObject(jArr.getJSONObject(index)));
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}
	
	private ShopInfosEntry parseJsonObject(JSONObject jItem) throws Exception {
		ShopInfosEntry item = new ShopInfosEntry();
		if(jItem.has("sid")) item.shopId = jItem.getInt("sid");
		if(jItem.has("title")) item.shopName =jItem.getString("title");
		if(jItem.has("thumb")) item.picThumb = (shopModel!=null?shopModel.picDomain:"") + jItem.getString("thumb");
		if(jItem.has("x") && jItem.has("y")){
			item.latlng = new LatLng(jItem.getDouble("x"), jItem.getDouble("y"));
		}
		item.address = jItem.getString("province")+jItem.getString("city")
					+jItem.getString("district")+jItem.getString("street") + jItem.getString("num");
		if(jItem.has("tel")) item.phone = String.valueOf(jItem.getLong("tel"));
		return item;
	}
	
	public ShopInfosModel getResponseObject(){
		return this.shopModel;
	}

}
