package com.pioneer.fationservice.parsers;

import org.json.JSONObject;

import com.pioneer.fationservice.entries.UsrInfoEntry;
import com.pioneer.fationservice.models.UsrinfoModel;

/**
 * 获取用户信息
 * @author Mrper
 *
 */
public class UsrinfoParser extends BaseParser {

	private UsrinfoModel model;
	
	public UsrinfoParser(String requestResult) {
		super(UsrinfoModel.class, requestResult);
	}

	@Override
	public void parseJSONModels(Object jsonModels, boolean isJSONObject) {
		try{
			this.model = (UsrinfoModel) super.getResponseObject();
			JSONObject jItem = (JSONObject) jsonModels;
			if(jItem!=null){
				this.model.usrinfo = new UsrInfoEntry();
				if(jItem.has("uid")) this.model.usrinfo.usrId = Integer.valueOf(jItem.get("uid").toString());
				if(jItem.has("name")) this.model.usrinfo.usrName = jItem.getString("name");
				if(jItem.has("point")) this.model.usrinfo.usrScore = Integer.valueOf(jItem.get("point").toString());
				if(jItem.has("tel")) this.model.usrinfo.usrPhone = jItem.getString("tel");
				if(jItem.has("email")) this.model.usrinfo.usrEmail = jItem.getString("email");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public UsrinfoModel getResponseObject(){
		return this.model;
	}

}
