package com.pioneer.fationservice.parsers;

import org.json.JSONArray;
import org.json.JSONObject;

import com.pioneer.fationservice.commons.SystemData;
import com.pioneer.fationservice.models.ResponseObject;

/**
 * 登录验证解析器
 * @author Mrper
 *
 */
public class LoginParser extends BaseParser{

	public LoginParser( String requestResult) {
		super(ResponseObject.class, requestResult);
	}

	@Override
	public void parseJSONModels(Object jsonModels,boolean isJSONObject) {
		if(jsonModels!=null){
			try{
				JSONObject jObj = isJSONObject?(JSONObject)jsonModels 
						:((JSONArray)jsonModels).getJSONObject(0);
				if(jObj.has("uid")) SystemData.uid = Integer.valueOf(jObj.get("uid").toString());
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
	}

}
