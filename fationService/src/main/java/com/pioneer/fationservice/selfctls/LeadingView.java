package com.pioneer.fationservice.selfctls;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.Scroller;

import com.pioneer.fationservice.helpers.PhoneHelper;

/**
 * 展示视图
 * @author Mrper
 *
 */
public class LeadingView extends LinearLayout implements OnTouchListener{
	
	public static interface SelectedChangedListener{
		public void selectedChanged(int selection);
	}
	
	private List<ImageView> imglst = new ArrayList<ImageView>();//图片视图集合
	private int selection = 0;//默认选中
	private float mLastX = 0;//横向坐标
	private SelectedChangedListener listener;
	private Scroller scroller;//滚动器
	private int width=0,height=0;//屏幕尺寸
	
	public LeadingView(Context context) {
		super(context);
		init(context,null);
	}
	
	public LeadingView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context,attrs);
	}

	@SuppressLint("NewApi")
	public LeadingView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context,attrs);
	}
	
	@SuppressLint("ClickableViewAccessibility")
	public void init(Context context,AttributeSet attrs){
		scroller = new Scroller(context);
		this.setOrientation(LinearLayout.HORIZONTAL);
		this.setClickable(true);
		this.setOnTouchListener(this);
	}
	
	public void setOnSelectedChangedListener(SelectedChangedListener listener){
		this.listener = listener;
	}
	
	@SuppressLint("SdCardPath")
	public void setImageUrls(int... resIds){
		imglst.clear();//清空控件缓存
		DisplayMetrics dm = PhoneHelper.getScreenInfo(getContext());
		width = dm.widthPixels;//获取屏幕宽度
		height = dm.heightPixels;//获取屏幕高度
		for(int index = 0;index < resIds.length; index++){
			ImageView imgView = new ImageView(getContext());
			imgView.setScaleType(ScaleType.FIT_XY);
			imgView.setImageResource(resIds[index]);
			imgView.setBackgroundColor(Color.BLUE);
			this.addView(imgView);
			imglst.add(imgView);
		}
	}

	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		super.onLayout(changed, l, t, r, b);
		if(imglst.size()>0 && width!=0 && height!=0){
			for(int index=0;index<imglst.size();index++){
				imglst.get(index).layout(width*index, 0, width*(index+1), height);
			}
		}
	}
	
	@SuppressLint("ClickableViewAccessibility")
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		float x = event.getX();
		switch(event.getAction()){
		case MotionEvent.ACTION_DOWN:
			if(!scroller.isFinished())
				scroller.abortAnimation();
			mLastX = x;
			break;
		case MotionEvent.ACTION_MOVE:
			break;
		case MotionEvent.ACTION_UP:
			if(Math.abs(mLastX - x) > 50){
				if(!scroller.isFinished()) 
					scroller.abortAnimation();//终止动画
				if(mLastX - x > 0) nextImg();//下一张图片
				else preImg(); //上一张图片
				if(listener!=null) 
					listener.selectedChanged(selection);
				return true;
			}
			break;
		}
		return super.onTouchEvent(event);
	}
	
	@Override
	public void computeScroll() {
		super.computeScroll();
		if(scroller.computeScrollOffset()){
			scrollTo(scroller.getCurrX(), scroller.getCurrY());
			postInvalidate();
		}
	}
	
	public void startScroll(int dx){
		scroller.startScroll(getScrollX(), 0, dx, 0, 200);
		invalidate();
	}
	
	public void nextImg(){
		selection++;
		if(selection>imglst.size()-1){
			selection = imglst.size()-1;
			return;
		}
		startScroll(width);
	}
	
	public void preImg(){
		selection--;
		if(selection<0){
			selection=0;
			return;
		}
		startScroll(-width);
	}
	
	public int getImglstSize(){
		return imglst.size();
	}
	
}
