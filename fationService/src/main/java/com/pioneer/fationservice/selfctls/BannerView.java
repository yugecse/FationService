package com.pioneer.fationservice.selfctls;

import java.util.ArrayList;
import java.util.List;

import net.tsz.afinal.FinalBitmap;
import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.Scroller;

import com.pioneer.fationservice.R;
import com.pioneer.fationservice.commons.SystemData;
import com.pioneer.fationservice.helpers.PhoneHelper;

/**
 * 展示视图
 * @author Mrper
 *
 */
public class BannerView extends FrameLayout implements OnTouchListener{
	
	public static interface BannerViewListener{
		public void selectedChanged(int selection);
		public void imageClick(int selection);
	}
	
	private BannerViewListener listener;
	private List<ImageView> imglst = new ArrayList<ImageView>();//图片视图集合
	private int selection = 0;//默认选中
	private float mLastX = 0;//横向坐标
	private Scroller scroller;//滚动器
	private int width = 0,height = 0;//屏幕尺寸
	private ViewGroup parentView;
	
	public void setOnBannerViewLister(BannerViewListener listener){
		this.listener = listener;
	}
	
	public BannerView(Context context) {
		super(context);
		init(context,null);
	}
	
	public BannerView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context,attrs);
	}

	public BannerView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context,attrs);
	}
	
	@SuppressLint("ClickableViewAccessibility")
	public void init(Context context,AttributeSet attrs){
		scroller = new Scroller(context);
		this.setClickable(true);
		this.setOnTouchListener(this);
		DisplayMetrics dm = PhoneHelper.getScreenInfo(getContext());
		width = dm.widthPixels;
		height = dm.heightPixels;
	}
	
	@SuppressLint("SdCardPath")
	public void setImageUrls(String... urls){
		imglst.clear();//清空控件缓存
		for(int index = 0;index < urls.length; index++){
			ImageView imgView = new ImageView(getContext());
			imgView.setScaleType(ScaleType.CENTER_CROP);
			FinalBitmap finalBitmap = FinalBitmap.create(getContext());
			finalBitmap.configDiskCachePath(SystemData.CACHE_DIR)
				.configLoadfailImage(R.drawable.ic_launcher)
				.display(imgView, urls[index]);
			this.addView(imgView);
			imglst.add(imgView);
		}
	}
	
	@Override
	protected void onLayout(boolean changed, int left, int top, int right,int bottom) {
		super.onLayout(changed, left, top, right, bottom);
		if(imglst.size()>0 && width!=0 && height!=0){
			for(int index=0;index<imglst.size();index++){
				imglst.get(index).layout(width*index, 0, width*(index+1), height);
			}
		}
	}
	
	@Override
	public void computeScroll() {
		super.computeScroll();
		if(scroller.computeScrollOffset()){
			scrollTo(scroller.getCurrX(),scroller.getCurrY());
			postInvalidate();
		}
	}
	
	public void setParentView(ViewGroup view){
		parentView = view;
	}
	
	public void startScroll(int dx){
		scroller.startScroll(getScrollX(), 0, dx, 0, 100);
		invalidate();
	}
	
	public void nextImg(){
		selection++;
		if(selection>imglst.size()-1){
			selection = imglst.size()-1;
			return;
		}
		startScroll(width);
	}
	
	public void preImg(){
		selection--;
		if(selection<0){
			selection=0;
			return;
		}
		startScroll(-width);
	}
	
	public int getImglstSize(){
		return imglst.size();
	}

	@SuppressLint("ClickableViewAccessibility")
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		if(parentView!=null) 
			parentView.requestDisallowInterceptTouchEvent(true);
		float x = event.getX();
		switch(event.getAction()){
		case MotionEvent.ACTION_DOWN:
			if(!scroller.isFinished())
				scroller.abortAnimation();
			mLastX = x;
			break;
		case MotionEvent.ACTION_MOVE:
			break;
		case MotionEvent.ACTION_UP:
			int dx = (int) Math.abs(mLastX - x);
			if(dx > 30){
				if(!scroller.isFinished())
					scroller.abortAnimation();
				if(mLastX - x > 0) nextImg();//下一张图片
				else preImg(); //上一张图片
				if(listener!=null)
					listener.selectedChanged(selection);
				return true;
			}
			if(listener!=null)
				listener.imageClick(selection);
			break;
		}
		return true;
	}
	
}
