package com.pioneer.fationservice.selfctls;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;

/**
 * 内嵌式的ListView
 * @author Mrper
 *
 */
public class InnerListView extends ListView {

	public InnerListView(Context context) {
		super(context);
	}

	public InnerListView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public InnerListView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE>>2, MeasureSpec.AT_MOST));
	}
}
