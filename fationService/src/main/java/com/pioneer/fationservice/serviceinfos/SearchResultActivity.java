package com.pioneer.fationservice.serviceinfos;

import net.tsz.afinal.FinalActivity;
import net.tsz.afinal.annotation.view.ViewInject;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.pioneer.fationservice.R;
import com.pioneer.fationservice.adapters.ShopInfoListAdapter;
import com.pioneer.fationservice.commons.SystemData;
import com.pioneer.fationservice.receivers.LocationBroadcastReceiver;
import com.pioneer.fationservice.receivers.LocationBroadcastReceiver.OnLocationChangedListener;

/**
 * 搜索结果页面
 * @author Mrper
 *
 */
public class SearchResultActivity extends FinalActivity implements OnItemClickListener{
	
	@ViewInject(id=R.id.activity_search_result_imgBack,click="onBackClick") private ImageView imgBack;//返回操作
	@ViewInject(id=R.id.activity_search_result_plLst) private PullToRefreshListView plLst;//刷新列表控件
	@ViewInject(id=R.id.activity_search_result_txtLocation) private TextView txtLocation;//当前位置文本
	@ViewInject(id=R.id.activity_search_result_etSearch) private EditText etSearch;//搜索框
	@ViewInject(id=R.id.activity_search_result_imgSearch,click="onSearchClick") private ImageView imgSearch;//搜索按钮
	
	private LocationBroadcastReceiver locationReceiver;//地理位置接收器
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search_result);
		initLocationReceiver();//注册地理接收器
		txtLocation.setText("当前位置："+SystemData.myCurrentLocation);
		plLst.setMode(Mode.PULL_FROM_END);//设置可以上拉
		plLst.setOnRefreshListener(new OnRefreshListener<ListView>() {
			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				plLst.onRefreshComplete();
			}
		});
		plLst.setAdapter(new ShopInfoListAdapter(this,null));
		plLst.setOnItemClickListener(this);
	}
	
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,long id) {
		startActivity(new Intent(this,ShopDetailActivity.class));
		overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
	}
	
	/**
	 * 搜索点击事件
	 * @param view
	 */
	public void onSearchClick(View view){
	}
	
	/**
	 * 返回操作  
	 * @param view
	 */
	public void onBackClick(View view){
		this.finish();
		overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
	}
	
	
	/**
	 * 初始化位置广播器
	 */
	public void initLocationReceiver(){
		locationReceiver = new LocationBroadcastReceiver();
		locationReceiver.setOnLocationChangedListener(new OnLocationChangedListener() {
			@Override
			public void locationChanged(String location) {
				txtLocation.setText("当前位置："+ location);
			}
		});
		IntentFilter filter = new IntentFilter(LocationBroadcastReceiver.ACTION_TAG);
		this.registerReceiver(locationReceiver,filter);
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		this.unregisterReceiver(locationReceiver);
	}
	
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
    	if(event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_BACK){
    		imgBack.performClick();//模拟点击返回按钮
    		return true;
    	}
    	return super.onKeyDown(keyCode, event);
    }
	
}
