package com.pioneer.fationservice.serviceinfos;

import net.tsz.afinal.FinalActivity;
import net.tsz.afinal.annotation.view.ViewInject;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.view.KeyEvent;
import android.view.Window;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Toast;

import com.baidu.android.pushservice.PushConstants;
import com.baidu.android.pushservice.PushManager;
import com.pioneer.fationservice.R;
import com.pioneer.fationservice.commons.SystemConstants;
import com.pioneer.fationservice.fragments.FragmentHome;
import com.pioneer.fationservice.fragments.FragmentMain;
import com.pioneer.fationservice.fragments.FragmentMore;
import com.pioneer.fationservice.fragments.FragmentRepair;
import com.pioneer.fationservice.fragments.FragmentUsrinfo;
import com.pioneer.fationservice.fragments.FragmentMain.onSelectedTouristListener;

public class MainActivity extends ActionBarActivity implements onSelectedTouristListener{
	
	@ViewInject(id=R.id.activity_main_rgMenu) private RadioGroup rgMenu;//菜单
	
	private long exitTime = 0;//退出时刻
	private Fragment fragmentHome,//首页
					 fragmentRepair,//维修点
					 fragmentUsrinfo,//用户中心
					 fragmentMore;//更多功能
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main);
		FinalActivity.initInjectedView(this);//进行视图映射
		displayFragment(R.id.activity_main_rgMenu_home);//初始化时显示首页
		rgMenu.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				displayFragment(checkedId);
			}
		});
		//开启PUSH Service
		PushManager.startWork(this, PushConstants.LOGIN_TYPE_API_KEY, SystemConstants.PUSH_APP_KEY);
	}
	
	/**
	 * 显示制定的Fragment
	 * @param radioId
	 */
	public void displayFragment(int radioId){
		FragmentManager fm = getSupportFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		hideAllFragment(fm, ft);
		ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
		switch(radioId){
		case R.id.activity_main_rgMenu_home:
			showFragment(ft,FragmentMain.class,fragmentHome,FragmentMain.TAG);
			break;
		case R.id.activity_main_rgMenu_repair:
			showFragment(ft,FragmentRepair.class,fragmentRepair,FragmentRepair.TAG);
			break;
		case R.id.activity_main_rgMenu_usrinfo:
			showFragment(ft,FragmentUsrinfo.class,fragmentUsrinfo,FragmentUsrinfo.TAG);
			break;
		case R.id.activity_main_rgMenu_more:
			showFragment(ft,FragmentMore.class,fragmentMore,FragmentMore.TAG);
			break;
		}
		ft.commit();
	}
	
	/**
	 * 显示Fragment
	 * @param ft
	 * @param cls
	 * @param fragment
	 * @param tag
	 */
	public void showFragment(FragmentTransaction ft,Class<? extends Fragment> cls,Fragment fragment,String tag){
		if(fragment == null){
			try {
				fragment = cls.newInstance();
				ft.add(R.id.activity_main_flContainer,fragment, tag);
			} catch (Exception e) {
				e.printStackTrace();
			}			
		}else{
			ft.show(fragment);
		}
	}
	
	/**
	 * 隐藏所有Fragment
	 * @param fm
	 * @param ft
	 */
	public void hideAllFragment(FragmentManager fm,FragmentTransaction ft){
		fragmentHome = fm.findFragmentByTag(FragmentMain.TAG);
		if(fragmentHome!=null) ft.hide(fragmentHome);
		fragmentRepair = fm.findFragmentByTag(FragmentRepair.TAG);
		if(fragmentRepair!=null) ft.hide(fragmentRepair);
		fragmentUsrinfo = fm.findFragmentByTag(FragmentUsrinfo.TAG);
		if(fragmentUsrinfo!=null) ft.hide(fragmentUsrinfo);
		fragmentMore = fm.findFragmentByTag(FragmentMore.TAG);
		if(fragmentMore!=null) ft.hide(fragmentMore);
	}
	

	@Override
	public void onSelectedTour() {
		((RadioButton)findViewById(R.id.activity_main_rgMenu_repair)).setChecked(true);
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_BACK){
			long nowTime = System.currentTimeMillis();
			if(nowTime - exitTime > 800){
				exitTime = nowTime;
				Toast.makeText(this,"再按一次退出程序",Toast.LENGTH_SHORT).show();
				return true;
			}
			this.finish();
			System.exit(0);
			return true;
		}else if(event.getAction() == KeyEvent.ACTION_DOWN && keyCode ==KeyEvent.KEYCODE_MENU) {
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

}
