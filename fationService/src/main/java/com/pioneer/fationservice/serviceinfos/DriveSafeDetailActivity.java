package com.pioneer.fationservice.serviceinfos;

import com.pioneer.fationservice.R;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import net.tsz.afinal.FinalActivity;
import net.tsz.afinal.annotation.view.ViewInject;

/**
 * 
 * @author Mrper
 *
 */
public class DriveSafeDetailActivity extends FinalActivity {
		
	@ViewInject(id=R.id.layout_actionbar_txtTitle) private TextView txtTitle;//标题栏
	@ViewInject(id=R.id.activity_drive_safe_detail_imgBack,click="onBackClick") 
	private ImageView imgBack;//返回操作
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_drive_safe_detail);
		txtTitle.setText("行车安全");
	}
	
	/**
	 * 返回操作
	 * @param view
	 */
	public void onBackClick(View view){
		this.finish();
		overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
	}
	
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
    	if(event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_BACK){
    		imgBack.performClick();//模拟点击返回按钮
    		return true;
    	}
    	return super.onKeyDown(keyCode, event);
    }
	
}
