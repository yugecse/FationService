package com.pioneer.fationservice.serviceinfos;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.pioneer.fationservice.R;
import com.pioneer.fationservice.adapters.ShopInfoListAdapter;
import com.pioneer.fationservice.commons.SystemData;
import com.pioneer.fationservice.commons.SystemHttpURL;
import com.pioneer.fationservice.entries.ShopInfoListEntry;
import com.pioneer.fationservice.helpers.ImageHelper;
import com.pioneer.fationservice.helpers.PhoneHelper;
import com.pioneer.fationservice.models.ShopInfoListModel;
import com.pioneer.fationservice.parsers.ShopInfoListParser;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import net.tsz.afinal.FinalActivity;
import net.tsz.afinal.FinalHttp;
import net.tsz.afinal.annotation.view.ViewInject;
import net.tsz.afinal.http.AjaxCallBack;
import net.tsz.afinal.http.AjaxParams;

/**
 * 商家列表页面
 * @author Mrper
 *
 */
public class ShoplstInfoActivity extends FinalActivity implements OnItemClickListener{
	
	@ViewInject(id=R.id.layout_actionbar_txtTitle) private TextView txtTitle;//标题栏
	@ViewInject(id=R.id.activity_shoplst_info_imgBack,click="onBackClick") private ImageView imgBack;//返回操作
	@ViewInject(id=R.id.activity_shoplst_info_plLst) PullToRefreshListView plLst;//维修店展示列表
	
	private int flag = 0;//商家类型
	private int pageIndex = 0;//当前页码
	private List<ShopInfoListEntry> shopInfoList = new ArrayList<ShopInfoListEntry>();//商家列表
	private ShopInfoListAdapter adapter;//商家数据适配器
	private ProgressDialog dialog;//进度提示框
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_shoplst_info);
		initActivity();//初始化activity的数据
		plLst.setMode(Mode.BOTH);//设置可以上拉，可以下啦
		plLst.setOnRefreshListener(new OnRefreshListener2<ListView>() {
			@Override
			public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
				requestShopList(1,false,true);//请求商家列表
			}
			@Override
			public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
				requestShopList(pageIndex+1,false,false);//请求商家列表
			}
		});
		adapter  = new ShopInfoListAdapter(this,shopInfoList);//商家地址
		plLst.setAdapter(adapter);
		plLst.setOnItemClickListener(this);
		requestShopList(1,true,true);//请求商家列表
	}

	
	/**
	 * 初始化activity
	 */
	public void initActivity(){
		Intent intent = getIntent();
		if(intent!=null && intent.hasExtra("flag")){
			flag = intent.getIntExtra("flag", 0);
			switch(flag){
			case 1:
				txtTitle.setText("美食");
				break;
			case 2:
				txtTitle.setText("酒店");
				break;
			case 3:
				txtTitle.setText("景点");
				break;
			}
		}
	}
	
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,long id) {
		Intent intent = new Intent(this,ShopDetailActivity.class);
		final ShopInfoListEntry item = shopInfoList.get(position-1);
		intent.putExtra("shopId", item.shopId);
		intent.putExtra("flag", flag);
		startActivity(intent);
		this.overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
	}
	
	/**
	 * 清空商家列表
	 */
	public void clearShoplst(){
		shopInfoList.clear();
		adapter.notifyDataSetChanged();
	}
	
	/**
	 * 返回操作
	 * @param view
	 */
	public void onBackClick(View view){
		this.finish();
		overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
	}
	
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
    	if(event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_BACK){
    		imgBack.performClick();//模拟点击返回按钮
    		return true;
    	}
    	return super.onKeyDown(keyCode, event);
    }
    
	/**
	 * 请求商家列表
	 */
	public void requestShopList(final int pageIndex,final boolean isFirstQuery,final boolean isClearlst){
		final Activity activity = this;
		if(isFirstQuery) dialog = ProgressDialog.show(activity, "", "正在获取数据");
    	FinalHttp finalHttp = new FinalHttp();
    	finalHttp.configCharset("UTF-8");
    	finalHttp.configTimeout(10*1000);
    	finalHttp.addHeader("Content-Type", "html/text");
    	finalHttp.addHeader("Conection", "Keep-Alive");
    	finalHttp.addHeader("Cache-Control", "no-cache");
    	HashMap<String,String> params = new HashMap<String,String>();
    	params.put("z", SystemData.myLatLng.longitude+","+SystemData.myLatLng.latitude);
    	params.put("p", String.valueOf(pageIndex));
    	params.put("f", String.valueOf(flag));
    	params.put("authkey", SystemData.HTTP_KEY);
    	finalHttp.post(SystemHttpURL.URL_SHOP_LIST, new AjaxParams(params),new AjaxCallBack<String>(){
    		@Override
    		public void onFailure(Throwable t, int errorNo, String strMsg) {
    			super.onFailure(t, errorNo, strMsg);
    			if(dialog!=null) dialog.dismiss();
    			if(plLst.isRefreshing()) plLst.onRefreshComplete();
    			Toast.makeText(activity, "网络错误，请重试!", Toast.LENGTH_SHORT).show();
    		}
    		@Override
    		public void onSuccess(String str) {
    			super.onSuccess(str);
    			if(dialog!=null) dialog.dismiss();
    			ShopInfoListModel model = (ShopInfoListModel) new ShopInfoListParser(str).getResponseObject();
    			switch(model.status){
    			case 1:
    				if(isClearlst) clearShoplst();
    				shopInfoList.addAll(model.shopLists);
    				adapter.notifyDataSetChanged();
    				ShoplstInfoActivity.this.pageIndex = pageIndex;//成功后获取当前页码
    				break;
    			case 0:
    				Toast.makeText(activity, model.msg, Toast.LENGTH_SHORT).show();
    				break;
    			}
    			if(plLst.isRefreshing()) plLst.onRefreshComplete();
    		}
    	});
	}	
	
	
}
