package com.pioneer.fationservice.serviceinfos;

import java.util.HashMap;

import net.tsz.afinal.FinalActivity;
import net.tsz.afinal.FinalHttp;
import net.tsz.afinal.annotation.view.ViewInject;
import net.tsz.afinal.http.AjaxCallBack;
import net.tsz.afinal.http.AjaxParams;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.pioneer.fationservice.R;
import com.pioneer.fationservice.commons.SystemData;
import com.pioneer.fationservice.commons.SystemHttpURL;
import com.pioneer.fationservice.models.DocinfoDetailModel;
import com.pioneer.fationservice.parsers.DocinfoDetailParser;

/**
 * 
 * @author Mrper
 *
 */
public class DocDetailActivity extends FinalActivity {
		
	@ViewInject(id=R.id.layout_actionbar_txtTitle) private TextView txtTitle;//标题栏
	@ViewInject(id=R.id.activity_doc_detail_imgBack,click="onBackClick") 
	private ImageView imgBack;//返回操作
	@ViewInject(id=R.id.activity_doc_detail_wvDetailContent)
	private WebView webView;//详细知识的内容浏览器
	
	private int flag = 0;//类型标识
	private int docId = 0;//文档ID
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_doc_detail);
		initActivity();//初始化activity
		initCtls();//初始化控件
		requestDocDetailInfo();//请求知识小文档详细内容
	}
	
	/**
	 * 初始化activity
	 */
	public void initActivity(){
		Intent intent = getIntent();
		if(intent!=null && intent.hasExtra("flag") && intent.hasExtra("doc_id")){
			flag = intent.getIntExtra("flag", 0);
			switch(flag){
			case 1:
				txtTitle.setText("行车安全");
				break;
			case 2:
				txtTitle.setText("汽车小知识");
				break;
			case 3:
				txtTitle.setText("故障应急");
				break;
			}
			docId = intent.getIntExtra("doc_id", 0);
		}
	}
	
	/**
	 * 初始化控件
	 */
	@SuppressLint("SetJavaScriptEnabled") 
	public void initCtls(){
		android.webkit.WebSettings vSetting = webView.getSettings();
		vSetting.setJavaScriptEnabled(true);
		vSetting.setDefaultTextEncodingName("UTF-8");
		vSetting.setBuiltInZoomControls(false);
	}
	
	/**
	 * 返回操作
	 * @param view
	 */
	public void onBackClick(View view){
		this.finish();
		overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
	}
	
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
    	if(event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_BACK){
    		imgBack.performClick();//模拟点击返回按钮
    		return true;
    	}
    	return super.onKeyDown(keyCode, event);
    }
    
    /**
     * 请求文档详细内容
     */
    public void requestDocDetailInfo(){
		final ProgressDialog dialog = ProgressDialog.show(this, "", "请稍等...");
		FinalHttp finalHttp = new FinalHttp();
    	finalHttp.configCharset("UTF-8");
    	finalHttp.configTimeout(10*1000);
    	finalHttp.addHeader("Content-Type", "html/text");
    	finalHttp.addHeader("Conection", "Keep-Alive");
    	finalHttp.addHeader("Cache-Control", "no-cache");
    	HashMap<String,String> params = new HashMap<String,String>();
    	params.put("did", String.valueOf(docId));
    	params.put("authkey", SystemData.HTTP_KEY);
    	finalHttp.post(SystemHttpURL.URL_DOC_CONTENT, new AjaxParams(params),new AjaxCallBack<String>(){
    		@Override
    		public void onFailure(Throwable t, int errorNo, String strMsg) {
    			super.onFailure(t, errorNo, strMsg);
    			if(dialog!=null) dialog.dismiss();
    			Toast.makeText(DocDetailActivity.this, "网络错误，请重试!", Toast.LENGTH_SHORT).show();
    		}
    		@Override
    		public void onSuccess(String str) {
    			super.onSuccess(str);
    			if(dialog!=null) dialog.dismiss();
    			System.out.println("小知识文档详细信息----返回信息："+str);
    			DocinfoDetailModel model = (DocinfoDetailModel) new DocinfoDetailParser(str).getResponseObject();
    			switch(model.status){
    			case 1:
    				webView.loadDataWithBaseURL("http://null",model.content, "text/html", "UTF-8",null);
    				break;
    			case 0:
    				Toast.makeText(DocDetailActivity.this, model.msg, Toast.LENGTH_SHORT).show();
    				break;
    			}
    		}
    	});
	}
	
}
