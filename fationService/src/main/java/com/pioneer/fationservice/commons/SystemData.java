package com.pioneer.fationservice.commons;

import android.annotation.SuppressLint;
import com.baidu.mapapi.model.LatLng;
import com.pioneer.fationservice.entries.UsrInfoEntry;

/**
 * 系统数据
 * @author Mrper
 *
 */
public class SystemData {
	
	/**
	 * 缓存目录,系统缓存目录
	 */
	@SuppressLint("SdCardPath") 
	public static String CACHE_DIR = "/fationservice/cache/";
	
	/**
	 * 访问权限KEY
	 */
	public static String HTTP_KEY = "";
	
	/**
	 * 当前用户是否已经登录
	 */
	public static boolean isLogin = false;
	
	/**
	 * 我的地理坐标
	 */
	public static LatLng myLatLng = null;
	
	/**
	 * 我当前的位置
	 */
	public static String myCurrentLocation = "";
	
	/**
	 * 用户信息
	 */
	public static int uid = 0;
	
}
