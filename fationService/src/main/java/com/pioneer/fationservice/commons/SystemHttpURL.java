package com.pioneer.fationservice.commons;

/**
 * 请求地址参数
 * @author Mrper
 *
 */
public class SystemHttpURL {

	/***
	 * 基础地址
	 */
	private static String BASE_URL = "http://xxx.shangzhicf.cn/";
	
	/**
	 * 请求验证码的地址
	 */
	public static String URL_SEND_VERFYCODE = BASE_URL + "user/sendMessage";
	
	/**
	 * 验证手机的地址
	 */
	public static String URL_VERFY_PHONE = BASE_URL + "user/verphone";
	
	/**
	 * 上传用户头像
	 */
	public static String URL_USER_UPLOAD_HEADER = BASE_URL +"user/uploadhead";
	
	/**
	 * 获取用户头像
	 */
	public static String URL_USER_GET_HEADER = BASE_URL + "user/gethead";
	
	/**
	 * 获取用户信息
	 */
	public static String URL_USER_GET_INFO = BASE_URL + "user/getuser";
	
	/**
	 * 商家列表的地址
	 */
	public static String URL_SHOP_LIST = BASE_URL + "shop/getShopList";
	
	/**
	 * 地图上的商家列表地址
	 */
	public static String URL_SHOP_MAP_LIST = BASE_URL + "shop/getShopPoint";
	
	/**
	 * 获取商铺详细信息
	 */
	public static String URL_SHOP_DETAIL_INFO = BASE_URL + "shop/getShop";
	
	/**
	 * 获取商铺弹窗信息
	 */
	public static String URL_SHOP_ALERT_INFO = BASE_URL + "shop/getShopMessage";
	
	/**
	 * 获取商家收藏列表
	 */
	public static String URL_SHOP_COLLECT_LIST = BASE_URL + "shop/getCollectList";
	
	/***
	 * 取消收藏和收藏商家
	 */
	public static String URL_SHOP_COLLECT = BASE_URL + "shop/collect";
	
	/**
	 * 知识小文档接口，f 1行车安全 2汽车小知识 3故障应急
	 */
	public static String URL_DOC_LIST = BASE_URL + "doc/getDoclist";
	
	/***
	 * 知识小文档详细内容
	 */
	public static String URL_DOC_CONTENT = BASE_URL + "doc/GetDocHhtml";
	
	/**
	 * 投诉和建议
	 */
	public static String URL_COMPLAINT = BASE_URL + "complaint/complaint";
	
}