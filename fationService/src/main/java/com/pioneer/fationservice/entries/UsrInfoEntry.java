package com.pioneer.fationservice.entries;


/**
 * 用户信息
 * @author Mrper
 *
 */
public class UsrInfoEntry{

	/**
	 * 用户ID
	 */
	public int usrId = 0;
	
	/**
	 * 用户昵称
	 */
	public String usrName = "";
	
	/**
	 * 用户电话号码
	 */
	public String usrPhone  = "";
	
	/**
	 * 用户积分
	 */
	public int usrScore;
	
	/**
	 * 用户邮箱
	 */
	public String usrEmail;
	
}
