package com.pioneer.fationservice.entries;

/**
 * 收藏列表实体类
 * @author Mrper
 *
 */
public class CollectInfoListEntry {

	/**
	 * 商家ID
	 */
	public int shopId;
	
	/**
	 * 商家名称
	 */
	public String shopName;
	
	/**
	 * 商家图片
	 */
	public String shopPic;
	
	/***
	 * 商家地址
	 */
	public String shopAddress;
	
	/**
	 * 商家电话
	 */
	public String shopPhone;
	
}
