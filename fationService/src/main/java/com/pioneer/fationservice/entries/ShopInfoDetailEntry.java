package com.pioneer.fationservice.entries;

import java.util.ArrayList;
import java.util.List;

import com.baidu.mapapi.model.LatLng;

/**
 * 商家详细信息实体数据模型
 * @author Mrper
 *
 */
public class ShopInfoDetailEntry {
	/**
	 * 商家ID
	 */
	public int shopId;
	/**
	 * 商家名称
	 */
	public String shopName;
	/**
	 * 商家描述
	 */
	public String shopDescription;
	/**
	 * 商家地址
	 */
	public String shopAddress;
	/**
	 * 商家电话
	 */
	public String shopPhone;
	/**
	 * 商家的经纬度
	 */
	public LatLng shopLatlng;
	/**
	 * 该商家是否已经收藏
	 */
	public boolean isCollect;
	/**
	 * 商家服务
	 */
	public String shopServer;
	/**
	 * 商家积分
	 */
	public String shopScore;
	/**
	 * 营业时间
	 */
	public String shopTime;
	/**
	 * 商家预约
	 */
	public String shopYuyue;
	/**
	 * 商家提示
	 */
	public String shopTip; 
	/**
	 * 商家官网
	 */
	public String shopHttp;
	/**
	 * 图片列表
	 */
	public List<String> piclst = new ArrayList<String>();
	
}
