package com.pioneer.fationservice.entries;

/**
 * 地图中的标识的实体类
 * @author Mrper
 *
 */
public class ShopMarkerInfoEntry {
	/**
	 * 商家ID
	 */
	public int shopId;
	/**
	 * 商家类型
	 */
	public int shopType;
	/**
	 * 商家名称
	 */
	public String shopName;
	/**
	 * 商家地址
	 */
	public String shopAddr;
	/**
	 * 商家电话
	 */
	public String shopCall;
	/**
	 * 其他信息
	 */
	public String otherInfo;
	/**
	 * 展示图的URL
	 */
	public String picUrl;
}
