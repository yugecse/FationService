package com.pioneer.fationservice.entries;

/**
 * 文档小知识列表实体
 * @author Mrper
 *
 */
public class DoclstInfoEntry {

	/**
	 * 文档ID
	 */
	public int docId;
	
	/**
	 * 文档标题
	 */
	public String docTitle;
	
	/**
	 * 文档描述
	 */
	public String docDescription;
	
	/**
	 * 文档更新时间
	 */
	public String docTime;
	
}
