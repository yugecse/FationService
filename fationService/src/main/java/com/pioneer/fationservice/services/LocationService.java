package com.pioneer.fationservice.services;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.mapapi.model.LatLng;
import com.pioneer.fationservice.commons.SystemData;

/**
 * 地理位置服务
 * @author Mrper
 *
 */
public class LocationService extends Service {

	public LocationService(){
		
	}
	
    public LocationClient mLocationClient;//定位服务器
	
    private BDLocationListener mBDLocationListener = new BDLocationListener() {//定位监听器
		@Override
		public void onReceiveLocation(BDLocation location) {
			if(location == null) return;
			SystemData.myLatLng = new LatLng(location.getLatitude(),location.getLongitude());
			SystemData.myCurrentLocation = location.getAddrStr();
			Intent intent = new Intent(com.pioneer.fationservice.receivers.LocationBroadcastReceiver.ACTION_TAG);
			intent.putExtra("addr", SystemData.myCurrentLocation);
			sendBroadcast(intent);//发送广播到首页，更新当前地理位置
		}
	};
	
	/**
	 * 初始化定位服务器
	 */
	public void initLocCliet(){
		mLocationClient = new LocationClient(this);
        LocationClientOption locOption = new LocationClientOption();
        locOption.setCoorType("bd09ll");
        locOption.setIsNeedAddress(true);
        locOption.setOpenGps(true);
        locOption.setScanSpan(3*1000);
        locOption.setTimeOut(10*1000);
        mLocationClient.setLocOption(locOption);
        mLocationClient.registerLocationListener(mBDLocationListener);
        mLocationClient.requestLocation();
        mLocationClient.start();
	}
    
	@Override
	public IBinder onBind(Intent intent) {
		return new LocationBinder();
	}
	
	@Override
	public void onCreate() {
		super.onCreate();
		initLocCliet();//初始化地理位置服务
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		initLocCliet();//初始化地理位置服务
		return super.onStartCommand(intent, flags, startId);
	}
	
	public class LocationBinder extends Binder{
		public LocationService getServiceInstance(){
			return LocationService.this;
		}
	}

}
