package com.pioneer.fationservice.models;

import com.pioneer.fationservice.entries.UsrInfoEntry;

/**
 * 用户信息实体类
 * @author Mrper
 *
 */
public class UsrinfoModel extends ResponseObject {
	
	/**
	 * 用户信息
	 */
	public UsrInfoEntry usrinfo;
	
}
