package com.pioneer.fationservice.models;

import java.util.ArrayList;
import java.util.List;

import com.pioneer.fationservice.entry.ShopInfosEntry;

/**
 * 主页商家地址的数据模型
 * @author Mrper
 *
 */
public class ShopInfosModel extends ResponseObject {
	
	/**
	 * 图片域名
	 */
	public String picDomain;
	
	/**
	 * 商家列表
	 */
	public List<ShopInfosEntry> shopLists = new ArrayList<ShopInfosEntry>();
	
	
}
