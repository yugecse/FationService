package com.pioneer.fationservice.models;

import com.pioneer.fationservice.entries.ShopInfoDetailEntry;

/**
 * 商家详细信息
 * @author Mrper
 *
 */
public class ShopInfoDetailModel extends ResponseObject {

	/**
	 * 图片地址
	 */
	public String imgDomain;
	
	
	/**
	 * 商家详细信息
	 */
	public ShopInfoDetailEntry shopInfoDetail;
	
}
