package com.pioneer.fationservice.models;

import java.util.ArrayList;
import java.util.List;

import com.pioneer.fationservice.entries.CollectInfoListEntry;

/**
 * 收藏的商家的列表数据模型
 * @author Mrper
 *
 */
public class CollectInfoListModel extends ResponseObject {

	/**
	 * 图片地址头部
	 */
	public String imgdommain;
	
	/**
	 * 商家列表实体集合
	 */
	public List<CollectInfoListEntry> collectionInfoList = new ArrayList<CollectInfoListEntry>();
	
}
