package com.pioneer.fationservice.models;
import com.pioneer.fationservice.entries.MapShopInfoEntry;

/**
 * 地图商家信息弹窗数据模型
 * @author Mrper
 *
 */
public class MapShopInfoModel extends ResponseObject {
	/**
	 * 图片地址头部
	 */
	public String imgdommain;
	
	/**
	 * 地图商家弹窗信息
	 */
	public MapShopInfoEntry mapShopInfoEntry; 
}
