package com.pioneer.fationservice.models;

import java.util.ArrayList;
import java.util.List;

import com.pioneer.fationservice.entries.MapShopListEntry;

/**
 * 地图商家列表解析器
 * @author Mrper
 *
 */
public class MapShopListModel extends ResponseObject {

	/**
	 * 商家列表
	 */
	public List<MapShopListEntry> mapShoplst = new ArrayList<MapShopListEntry>();
	
}
