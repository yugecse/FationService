package com.pioneer.fationservice.models;

import java.util.ArrayList;
import java.util.List;

import com.pioneer.fationservice.entries.ShopInfoListEntry;

/**
 * 主页商家地址的数据模型
 * @author Mrper
 *
 */
public class ShopInfoListModel extends ResponseObject {
	
	/**
	 * 图片域名
	 */
	public String picDomain;
	
	/**
	 * 商家列表
	 */
	public List<ShopInfoListEntry> shopLists = new ArrayList<ShopInfoListEntry>();
	
	
}
