package com.pioneer.fationservice.models;

/**
 * 文档详细内容
 * @author Mrper
 *
 */
public class DocinfoDetailModel extends ResponseObject {
	/**
	 * ID号
	 */
	public int docId;
	
	/**
	 * 内容
	 */
	public String content;
}
