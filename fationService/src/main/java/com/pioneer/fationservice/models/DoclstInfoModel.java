package com.pioneer.fationservice.models;

import java.util.ArrayList;
import java.util.List;

import com.pioneer.fationservice.entries.DoclstInfoEntry;

/**
 * 文档列表信息数据模型
 * @author Mrper
 *
 */
public class DoclstInfoModel extends ResponseObject {

	/**
	 * 文档列表集合
	 */
	public List<DoclstInfoEntry> doclst = new ArrayList<DoclstInfoEntry>();
	
}
