package com.pioneer.fationservice.models;


/**
 * 返回结果父类
 * @author Mrper
 *
 */
public class ResponseObject {

	public ResponseObject(){
		
	}
	
	/**
	 * 返回的状态值
	 */
	public int status;
	/**
	 * 返回的信息
	 */
	public String msg;
	
}
