package com.pioneer.fationservice.selfviews;

import net.tsz.afinal.FinalBitmap;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.pioneer.fationservice.R;
import com.pioneer.fationservice.entries.ShopMarkerInfoEntry;

/**
 * 商家信息框
 * @author Mrper
 *
 */
public class ShopMarkerInfoDialog extends Dialog {
	
	private ImageView imgClose,//关闭
					  imgShopPic;//展示图;
	private TextView txtShopName,//商家名称
					 txtShopCall,//商家电话
					 txtShopAddr,//商家地址
					 txtShopOtherInfo,//其他信息
					 txtViewInfo,//查看详细信息
					 txtBack;//返回
	
	public void setOnOperClickListener(View.OnClickListener listener){
		//设置监听
		imgClose.setOnClickListener(listener);
		txtViewInfo.setOnClickListener(listener);
		txtBack.setOnClickListener(listener);
	}
	
	public ShopMarkerInfoDialog(Context context,ShopMarkerInfoEntry markerInfo) {
		super(context);
		init(markerInfo);//初始化操作
	}
	
	/**
	 * 初始化操作
	 */
	public void init(ShopMarkerInfoEntry markerInfo){
		this.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		this.setContentView(R.layout.dialog_maker_info);
		imgClose = (ImageView)findViewById(R.id.dialog_maker_info_imgClose);
		txtShopName = (TextView)findViewById(R.id.dialog_maker_info_txtTitle);
		imgShopPic = (ImageView)findViewById(R.id.dialog_maker_info_imgInfo);
		txtShopCall = (TextView)findViewById(R.id.dialog_maker_info_txtCall);
		txtShopAddr = (TextView)findViewById(R.id.dialog_maker_info_txtAddr);
		txtShopOtherInfo = (TextView)findViewById(R.id.dialog_maker_info_txtOtherInfo);
		txtViewInfo = (TextView)findViewById(R.id.dialog_maker_info_txtViewInfo);
		txtBack = (TextView)findViewById(R.id.dialog_maker_info_txtBack);
		//图片下载
		FinalBitmap.create(getContext()).display(imgShopPic, markerInfo.picUrl);
		//数据赋值
		txtShopName.setText(markerInfo.shopName);
		txtShopCall.setText("电话：" + markerInfo.shopCall);
		txtShopAddr.setText("地址：" + markerInfo.shopAddr);
		txtShopOtherInfo.setText("描述：" + markerInfo.otherInfo);
	}

}
