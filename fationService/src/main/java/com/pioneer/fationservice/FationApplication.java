package com.pioneer.fationservice;

import com.baidu.frontia.FrontiaApplication;
import com.baidu.mapapi.SDKInitializer;

public class FationApplication extends FrontiaApplication {
	
	@Override
	public void onCreate() {
		super.onCreate();
		//百度地图初始化调用的函数
		SDKInitializer.initialize(getApplicationContext());
	}
	
}
