package com.pioneer.fationservice.entry;

/**
 * 地图商家弹窗信息实体类
 * @author Mrper
 *
 */
public class MapShopInfoEntry {

	/**
	 * 商家ID
	 */
	public int shopId;
	
	/**
	 * 商家名称
	 */
	public String shopName;
	
	/**
	 * 商家缩图
	 */
	public String shopThumb;
	
	/**
	 * 商家电话
	 */
	public String shopPhone;
	
	/**
	 * 商家地址
	 */
	public String shopAddress;
	
	/**
	 * 商家描述
	 */
	public String shopDescription;
}
