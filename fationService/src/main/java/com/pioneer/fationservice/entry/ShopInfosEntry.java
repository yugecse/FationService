package com.pioneer.fationservice.entry;

import com.baidu.mapapi.model.LatLng;

/**
 * 商家列表的实体类
 * @author Mrper
 *
 */
public class ShopInfosEntry {
	/**
	 * 商家ID
	 */
	public int shopId;
	
	/**
	 * 商家名称
	 */
	public String shopName;
	
	/**
	 * 商家缩略图
	 */
	public String picThumb;
	
	/**
	 * 商家电话
	 */
	public String phone;
	
	/**
	 * 商家经纬度
	 */
	public LatLng latlng;
	
	/**
	 * 商家地址
	 */
	public String address;
}
