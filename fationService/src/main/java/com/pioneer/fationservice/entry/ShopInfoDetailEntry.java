package com.pioneer.fationservice.entry;

import java.util.ArrayList;
import java.util.List;

import com.baidu.mapapi.model.LatLng;

/**
 * 商家详细信息实体数据模型
 * @author Mrper
 *
 */
public class ShopInfoDetailEntry {
	/**
	 * 商家ID
	 */
	public int shopId;
	/**
	 * 商家名称
	 */
	public String shopName;
	/**
	 * 商家描述
	 */
	public String shopDescription;
	/**
	 * 商家地址
	 */
	public String shopAddress;
	/**
	 * 商家电话
	 */
	public String shopPhone;
	/**
	 * 商家的经纬度
	 */
	public LatLng shopLatlng;
	
	/**
	 * 图片列表
	 */
	public List<String> piclst = new ArrayList<String>();
	
}
