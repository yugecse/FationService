package com.pioneer.fationservice.entry;

import com.baidu.mapapi.model.LatLng;

/**
 * 地图商家列表实体类
 * @author Mrper
 *
 */
public class MapShopListEntry {

	/**
	 * 商家ID
	 */
	public int shopId;
	
	/**
	 * 商家的地理位置
	 */
	public LatLng shopLatLng;
	
}
