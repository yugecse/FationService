package com.pioneer.fationservice.entry;


/**
 * 用户信息
 * @author Mrper
 *
 */
public class UsrInfoEntry{

	/**
	 * 用户ID
	 */
	public int usrId = 0;
	
	/**
	 * 用户昵称
	 */
	public String usrName = "";
	
	/**
	 * 用户电话号码
	 */
	public String usrPhone  = "";
	
	/**
	 * 金币数量
	 */
	public int moneyCount = 0;
	
}
